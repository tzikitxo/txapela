#pragma once

#include <iostream>
#include <string>


template <typename T, size_t N>
using c_array = T[N];


//! Define the debug break using the MS specific or general using assembly interrupt 3
#ifdef _MSC_VER
#define G_DEBUG_BREAK __debugbreak()
#else
#define G_DEBUG_BREAK do { __asm int 3 } while(0)
#endif


//! By defining G_ENABLE_DEBUG_DIAGNOSTICS you can explicitly enable or disable debugging and diagnostic macros
#if !defined(G_ENABLE_DEBUG_DIAGNOSTICS) 
#   if defined(_DEBUG)
#       define G_ENABLE_DEBUG_DIAGNOSTICS 1
#   else
#       define G_ENABLE_DEBUG_DIAGNOSTICS 0
#   endif
#endif

#ifndef FOR_EACH
#define FOR_EACH(itName, container)												\
		for(auto itName = (container).begin(); itName != (container).end(); ++itName)	
#endif

#ifndef IE_ARRAYSIZE
#define IE_ARRAYSIZE(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))
#endif



//! Implement these functions to control how errors and debug printing are handled,
static bool SignalErrorHandler(const char * expression, const char * file, int line, const char * formatMessage = 0, ...)
{
	std::cout << "ERROR:" << expression <<  std::endl;
	std::cout << "\t" << "File:" << file << std::endl;
	std::cout << "\t" << "Line:" << line << std::endl;

	return true;
}

//! Implement these functions to control how errors and debug printing are handled,
static void DebugPrintHandler(const char * msg, ...) {}

#if G_ENABLE_DEBUG_DIAGNOSTICS

//! \def DebugPrint If diagnostics are enabled use the debug functions
#define DebugPrint(...) DebugPrintHandler( __VA_ARGS__ );

// The do/while blocks embedded here are ABSOLUTELY NECESSARY to prevent
// bizzare compilation errors. 
// Check out http://cnicholson.net/2009/02/stupid-c-tricks-adventures-in-assert/
// for more details.

#define ErrorIf(exp, ...) \
	do { if( (exp) && SignalErrorHandler(#exp, __FILE__, __LINE__,__VA_ARGS__ )) \
	G_DEBUG_BREAK; } while(0)

#define GAssert(exp, ...) \
	do { if( (!(exp)) && SignalErrorHandler(#exp, __FILE__, __LINE__,__VA_ARGS__ )) \
	G_DEBUG_BREAK; } while(0)

#else

//If the diagnostics are disabled all the debug functions
//are no ops

#define DebugPrint(...)  ((void)0)      
#define ErrorIf(...)	 ((void)0)      
#define GAssert(...)	 ((void)0)      

#endif
#ifndef DEPRECATED
#ifdef __GNUC__
#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define DEPRECATED __declspec(deprecated)
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED(func) func
#endif
#endif // !DEPRECATED

// Statements like:
//		#pragma message(Reminder "Fix this problem!")
// Which will cause messages like:
//		C:\Source\Project\main.cpp(47): Reminder: Fix this problem!
// to show up during compiles.  Note that you can NOT use the
// words "error" or "warning" in your reminders, since it will
// make the IDE think it should abort execution.  You can double
// click on these messages and jump to the line in question.
#ifndef TODO
#define Stringize( L )			#L
#define MakeString( M, L )		M(L)
#define $Line					\
	MakeString( Stringize, __LINE__ )

#define WarningHeader(WarningCode)			\
	__FILE__ "(" $Line ") : Warning " WarningCode " : "

#define WARNING(WarningCode, WarningText) __pragma(message(WarningHeader(WarningCode) WarningText))
#define TODO(WarningText) WARNING("TODO", WarningText)
#define HACK(WarningText) WARNING("HACK", WarningText)
#endif // !TODO