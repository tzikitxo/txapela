#pragma once

#include <string>

#include "DLL_EngineCore.hh"

class ORC_ENGINE RTTI
{
private:
	std::string mName;
	const RTTI * mpBaseType;

public:
	//! Constructor.
	RTTI(std::string & name, const RTTI * pBaseType);
	RTTI(const char * name, const RTTI * pBaseType);

	//! Gettor.
	const char * name() const;

	//! Compare.
	bool is_exactly(const RTTI & other) const;
	bool operator==(const RTTI& rhs);
	bool operator!=(const RTTI& rhs);

	//! Check derivation from.
	bool is_derived(const RTTI & other) const;

};

using Rtti = RTTI;

#define RTTI_DECL											\
	public:															\
		static const RTTI TYPE;									\
		virtual const RTTI& type() const						\
		{															\
			return TYPE;											\
		}	

#define RTTI_IMPL(thisType, parentTypeAddr)							\
	const RTTI thisType::TYPE = RTTI(typeid(thisType).name(), parentTypeAddr);	\


