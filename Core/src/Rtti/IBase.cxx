#include "IBase.hh"


namespace core
{
	RTTI_IMPL(IBase, nullptr);

	Json::Value IBase::serialize() const
	{
		return Json::Value();
	}

	void IBase::unserialize(const Json::Value & data)
	{
	}

	IBase::~IBase()
	{}
}