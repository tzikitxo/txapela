#pragma once

#include "RTTI.hh"
#include "DLL_EngineCore.hh"
#include "Serialization\json\json.h"

namespace core
{

	class ORC_ENGINE IBase
	{
	public:
		RTTI_DECL;

		//! Saves the data to a json type file.
		virtual Json::Value serialize() const;

		//! Loads data from a json type file.
		virtual void unserialize(const Json::Value & data);

	public:
		unsigned int ID{ ~0u };			//!< Unique ID of the class.

	protected:
		IBase() = default;
		virtual ~IBase() = 0;
	};

}