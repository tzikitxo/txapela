#include "Rtti.hh"


RTTI::RTTI(std::string & name, const RTTI * pBaseType)
{
	RTTI(name.c_str(), pBaseType);
}

RTTI::RTTI(const char * name, const RTTI * pBaseType)
{
	// store the name
	mName = name;
	// remove the "class " prefix, returned by typeid().name() function
	mName = mName.substr(mName.find_first_of(" ") + 1).c_str();

	// store the pointer to the base type
	mpBaseType = pBaseType;
}

const char * RTTI::name() const
{
	return mName.c_str();
}

bool RTTI::is_exactly(const RTTI & other) const
{
	return this == &other;
}

bool RTTI::operator==(const RTTI & rhs)
{
	return this == &rhs;
}

bool RTTI::operator!=(const RTTI & rhs)
{
	return this != &rhs;
}

bool RTTI::is_derived(const RTTI & other) const
{
	const RTTI * temp = this;
	while (temp)
	{
		if (temp == &other)
		{
			return true;
		}

		temp = temp->mpBaseType;
	}
	return false;
}