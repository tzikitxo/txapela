#pragma once

#include "Rtti\IBase.hh"
#include "Macros.h"

#include "DLL_EngineCore.hh"

#include <map>
#include <list>

namespace events
{
	struct ORC_ENGINE IEvent
	{
	public:
		IEvent(const char * id)
			: mID(id)
			, mHandled(false) {}
		~IEvent() {}

		// String id
		std::string mID;
		bool mHandled;
	};

	class ORC_ENGINE  IEventHandler
	{
	public:
		virtual void operator()() {};
		virtual void operator()(IEvent *e) {};
	};

	template <typename EVENT_TYPE>
	class  GlobalEventHandler : public IEventHandler
	{
	public:
		typedef void(*EVENT_CALLBACK)(EVENT_TYPE*);
		//protected:
		EVENT_CALLBACK pCB;
	public:
		GlobalEventHandler(EVENT_CALLBACK cb)
			: pCB(cb)
		{}
		virtual void operator()(IEvent *e)
		{
			CallType(static_cast<EVENT_TYPE*>(e));
		};
		void CallType(EVENT_TYPE* e) {
			if (pCB)
				pCB(e);
		}
	};

	template <typename HANDLER_TYPE,
		typename EVENT_TYPE >
		class  ObjEventHandler : public IEventHandler
	{
	public:
		typedef void(HANDLER_TYPE::* EVENT_CALLBACK)(EVENT_TYPE*);
		//protected:
		HANDLER_TYPE * pSender;
		EVENT_CALLBACK pCB;
	public:
		ObjEventHandler(HANDLER_TYPE *ht, EVENT_CALLBACK cb)
			: pSender(ht)
			, pCB(cb)
		{}
		virtual void operator()(IEvent *e)
		{
			CallType(static_cast<EVENT_TYPE*>(e));
		};
		virtual void CallType(EVENT_TYPE *e)
		{
			if (pCB && pSender)
			{
				//((thisPtr.lock().get())->*pCBType)(ev);
				((pSender)->*pCB)(e);
			}
		}
	};
	// ------------------------------------------------------------------------

	class ORC_ENGINE GlobalVoidEventHandler : public IEventHandler
	{
	public:
		typedef void(*EVENT_CALLBACK)();
		//protected:
		EVENT_CALLBACK pCB;
	public:
		GlobalVoidEventHandler(EVENT_CALLBACK cb) : pCB(cb) {}
		virtual void operator()() { if (pCB)	pCB(); }
		virtual void operator()(IEvent*) { if (pCB)	pCB(); }
	};

	template <typename HANDLER_TYPE>
	class  ObjVoidEventHandler : public IEventHandler
	{
	public:
		typedef void(HANDLER_TYPE::* EVENT_CALLBACK)();
		//protected:
		HANDLER_TYPE * pSender;
		EVENT_CALLBACK pCB;
	public:
		ObjVoidEventHandler(HANDLER_TYPE *ht, EVENT_CALLBACK cb)
			: pSender(ht)
			, pCB(cb)
		{}
		virtual void operator()()
		{
			if (pCB && pSender)
			{
				((pSender)->*pCB)();
			}
		}
		virtual void operator()(IEvent*)
		{
			if (pCB && pSender)
			{
				((pSender)->*pCB)();
			}
		}
	};

	class  ORC_ENGINE GMessageBox
	{
		static std::map<core::IBase *, std::map<std::string, std::list<IEventHandler*>>> mListeners;

	public:

		// ------------------------------------------------------------------------
		// Connect Global void functin to global event
		static void Connect(const char * eventId, GlobalVoidEventHandler::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			GlobalVoidEventHandler * geh = new GlobalVoidEventHandler(cb);
			mListeners[NULL][eventId].push_back(geh);
		}

		// ------------------------------------------------------------------------
		// Connect global void function to class event
		static void Connect(core::IBase * sender, const char * eventId, GlobalVoidEventHandler::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			GlobalVoidEventHandler * geh = new GlobalVoidEventHandler(cb);
			mListeners[sender][eventId].push_back(geh);
		}

		// ------------------------------------------------------------------------
		// Connect Global function to Global EVENT
		template<typename EVENT_TYPE>
		static void Connect(const char * eventId, typename GlobalEventHandler<EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			Connect<EVENT_TYPE>(NULL, eventId, cb);
		}

		// ------------------------------------------------------------------------
		// Connect global function to class event
		template<typename EVENT_TYPE>
		static void Connect(core::IBase * sender, const char * eventId, typename GlobalEventHandler<EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			GlobalEventHandler<EVENT_TYPE> * geh = new GlobalEventHandler<EVENT_TYPE>(cb);
			mListeners[sender][eventId].push_back(geh);
		}

		// ------------------------------------------------------------------------
		// Connect class void method to global event
		template<typename HANDLER_TYPE>
		static void ConnectObj(const char * eventId, HANDLER_TYPE* handler, typename ObjVoidEventHandler<HANDLER_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			ObjVoidEventHandler<HANDLER_TYPE> * geh = new ObjVoidEventHandler<HANDLER_TYPE>(handler, cb);
			mListeners[NULL][eventId].push_back(geh);
		}

		// ------------------------------------------------------------------------
		// Connect class void method to class event
		template<typename HANDLER_TYPE>
		static void ConnectObj(core::IBase * sender, const char * eventId, HANDLER_TYPE* handler, typename ObjVoidEventHandler<HANDLER_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			ObjVoidEventHandler<HANDLER_TYPE> * oeh = new ObjVoidEventHandler<HANDLER_TYPE>(handler, cb);
			mListeners[sender][eventId].push_back(oeh);
		}

		// ------------------------------------------------------------------------
		// Connect class method to global event
		template<typename HANDLER_TYPE, typename EVENT_TYPE>
		static void ConnectObj(const char * eventId, HANDLER_TYPE* handler, typename ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			ConnectObj<HANDLER_TYPE, EVENT_TYPE>(NULL, eventId, handler, cb);
		}

		// ------------------------------------------------------------------------
		// Connect class method to class event
		template<typename HANDLER_TYPE, typename EVENT_TYPE>
		static void ConnectObj(core::IBase * sender, const char * eventId, HANDLER_TYPE* handler, typename ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			ObjEventHandler<HANDLER_TYPE, EVENT_TYPE> * oeh = new ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>(handler, cb);
			mListeners[sender][eventId].push_back(oeh);
		}

		/********************************************/


		static void Disconnect(const char * eventId, GlobalVoidEventHandler::EVENT_CALLBACK cb)
		{
			Disconnect(NULL, eventId, cb);
		}

		// ------------------------------------------------------------------------
		// Disconnect global void function to class event
		static void Disconnect(core::IBase * sender, const char * eventId, GlobalVoidEventHandler::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			auto eventListenersIt = mListeners[sender].find(eventId);
			if (eventListenersIt != mListeners[sender].end())
			{

				FOR_EACH(it, eventListenersIt->second)
				{
					if ((*it) == (IEventHandler *)cb)
					{
						it = eventListenersIt->second.erase(it);
						return;
					}
				}
			}
		}

		// ------------------------------------------------------------------------
		// Disconnect Global function to Global EVENT
		template<typename EVENT_TYPE>
		static void Disconnect(const char * eventId, typename GlobalEventHandler<EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			Disconnect<EVENT_TYPE>(NULL, eventId, cb);
		}

		// ------------------------------------------------------------------------
		// Disconnect global function to class event
		template<typename EVENT_TYPE>
		static void Disconnect(core::IBase * sender, const char * eventId, typename GlobalEventHandler<EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			auto eventListenersIt = mListeners[sender].find(eventId);
			if (eventListenersIt != mListeners[sender].end())
			{

				FOR_EACH(it, eventListenersIt->second)
				{
					if ((*it) == (IEventHandler *)cb)
					{
						it = eventListenersIt->second.erase(it);
						return;
					}
				}
			}

		}

		template<typename HANDLER_TYPE>
		static void DisconnectObj(const char * eventId, HANDLER_TYPE* handler, typename ObjVoidEventHandler<HANDLER_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			ObjVoidEventHandler<HANDLER_TYPE> * geh = new ObjVoidEventHandler<HANDLER_TYPE>(handler, cb);
			mListeners[NULL][eventId].erase(geh);
		}

		template<typename HANDLER_TYPE>
		static void DisconnectObj(core::IBase * sender, const char * eventId, HANDLER_TYPE* handler, typename ObjVoidEventHandler<HANDLER_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			auto eventListenersIt = mListeners[sender].find(eventId);
			if (eventListenersIt != mListeners[sender].end())
			{

				FOR_EACH(it, eventListenersIt->second)
				{
					ObjVoidEventHandler<HANDLER_TYPE>* handlerPtr = (ObjVoidEventHandler<HANDLER_TYPE>*)(*it);
					if (handlerPtr->pSender == handler && handlerPtr->pCB == cb)
					{
						it = eventListenersIt->second.erase(it);
						return;
					}
				}
			}
		}

		template<typename HANDLER_TYPE, typename EVENT_TYPE>
		static void DisconnectObj(const char * eventId, HANDLER_TYPE* handler, typename ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			DisconnectObj<HANDLER_TYPE, EVENT_TYPE>(NULL, eventId, handler, cb);
		}

		template<typename HANDLER_TYPE, typename EVENT_TYPE>
		static void DisconnectObj(core::IBase * sender, const char * eventId, HANDLER_TYPE* handler, typename ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>::EVENT_CALLBACK cb)
		{
			if (!eventId)return;
			auto eventListenersIt = mListeners[sender].find(eventId);
			if (eventListenersIt != mListeners[sender].end())
			{

				FOR_EACH(it, eventListenersIt->second)
				{
					ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>* handlerPtr = (ObjEventHandler<HANDLER_TYPE, EVENT_TYPE>*)(*it);
					if (handlerPtr->pSender == handler && handlerPtr->pCB == cb)
					{
						it = eventListenersIt->second.erase(it);
						return;
					}
				}
			}
		}

		static void Dispatch(const char * eventId)
		{
			// Dispatch
			Dispatch(NULL, eventId);
		}
		static void Dispatch(core::IBase * sender, const char * eventId)
		{
			// sanity check
			if (!eventId)
				return;

			// find if the sender is registered
			auto senderHandlers = mListeners.find(sender);
			if (senderHandlers == mListeners.end())
				return;

			// find if any handler
			auto listenersIt = senderHandlers->second.find(eventId);
			if (listenersIt != senderHandlers->second.end())
			{
				FOR_EACH(it, listenersIt->second)
				{
					// handle
					(*(*it))();
				}
			}
		}
		template<typename EVENT_TYPE>
		static void Dispatch(const char * eventId, EVENT_TYPE *e)
		{
			// Dispatch
			Dispatch<EVENT_TYPE>(NULL, eventId, e);
		}
		template<typename EVENT_TYPE>
		static void Dispatch(core::IBase * sender, const char * eventId, EVENT_TYPE *e)
		{
			// sanity check
			// if (!eventId || !sender)
			// 	return; 

			// find if the sender is registered
			auto senderHandlers = mListeners.find(sender);
			if (senderHandlers == mListeners.end())
				return;

			// find if any handler
			auto listenersIt = senderHandlers->second.find(eventId);
			if (listenersIt != senderHandlers->second.end())
			{
				FOR_EACH(it, listenersIt->second)
				{
					// handle
					(*(*it))(e);
				}
			}
		}
	};
} // namespace event.