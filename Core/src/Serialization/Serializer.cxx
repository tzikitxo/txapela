#include "Serializer.hh"
#include <iostream>
#include <fstream>



Json::Value serializer::load_file(const char * filename)
{
	Json::Value document;
	std::ifstream infile(filename);
	if (infile.is_open())
	{
		Json::Reader jsonReader;
		if (!jsonReader.parse(infile, document))
		{
			std::cout << "Serializer::LoadFile - Error parsing file: " << filename << std::endl;
		}
	}
	else
	{
		std::cout << "Serializer::LoadFile - Error reading file: " << filename << std::endl;
	}
	return document;
}

void serializer::save_file(const char * filename, Json::Value & value)
{
	std::ofstream out(filename, std::ofstream::out);
	if (out.is_open())
	{
		Json::StyledWriter jsonWriter;
		out << jsonWriter.write(value);
	}
	else
	{
		std::cout << "Serializer::WriteToFile - Could not save to file" << std::endl;
	}

}
