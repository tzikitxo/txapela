#pragma once

#include <string>
#include "json\json.h"

namespace serializer
{
	Json::Value load_file(const char * filename);
	void save_file(const char * filename, Json::Value & value);

} // !Serializer namespace.