#include "ISystem.hh"

application::ISystem::ISystem(std::function<void()> init, fn_ptr<void()> update, fn_ptr<void()> frame, fn_ptr<void()> shutdown)
	: mInit(std::move(init))
	, mUpdate(update)
	, mFrame(frame)
	, mShutdown(shutdown)
{}

void application::ISystem::initialize()
{
	this->mInit();
}

void application::ISystem::update()
{
	if (!this->mSleep)
		this->mUpdate();
}

void application::ISystem::frame_end()
{
	if (!this->mSleep)
		this->mFrame();
}

void application::ISystem::shutdown()
{
	this->mShutdown();
}

void application::ISystem::sleep() noexcept
{
	this->mSleep = true;
}

void application::ISystem::resume() noexcept
{
	this->mSleep = false;
}

bool application::ISystem::is_sleep() noexcept
{
	return this->mSleep;
}

void application::ISystem::dummy() noexcept
{
}


