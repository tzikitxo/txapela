#pragma once



#include <Rtti\IBase.hh>
#include "Events\Events.hh"
#include <DLL_EngineCore.hh>


namespace application
{
	//! Global event to call for system pause.
	struct ORC_ENGINE PauseEvent : public events::IEvent
	{
		PauseEvent() :IEvent("PauseEvent") {}
	};

	struct ORC_ENGINE ChangeLvlEvent : public events::IEvent
	{
		ChangeLvlEvent() :IEvent("ChangeLvlEvent") {}
		ChangeLvlEvent(unsigned int next) : IEvent("ChangeLvlEvent") { eNextLvl = next; }
		unsigned int eNextLvl;
	};

	class ORC_ENGINE AppState : public core::IBase
	{
	public:
		RTTI_DECL;

		AppState() = default;
		virtual ~AppState() = default;

		virtual void load() = 0;
		virtual void unload() = 0;
		virtual void initialize() = 0;
		virtual void free() = 0;
		virtual void update() = 0;
	};


} // namespace application
