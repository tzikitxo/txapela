
#include "Application.hh"
#include "Events\Events.hh"

#include "Utils\Iterators.hh"
#include <memory>
#include "ResourceManager\ResourceManager.hh"
#include <Window\Window.hh>
#include "..\GUI\GuiSystem.hh"


namespace application
{
	namespace
	{
		namespace status
		{
			const RTTI* status_default = nullptr;
			const RTTI* status_restart = reinterpret_cast<const RTTI *>(0x00000001);
			const RTTI* status_quit = reinterpret_cast<const RTTI *>(0x00000002);
		}

		struct impl
		{
			std::vector<ISystem> mSystems; //!< Systems on the application.
			bool mPaused{ false };

			std::map<const RTTI*, AppState*> mStates; //!< Map with states ordered with their rtti.

			AppState* mCurrent = nullptr;
			const RTTI * mPreviousState = status::status_default;					 //! Type of the previous state
			const RTTI * mNextState = status::status_default;						 //! Type of the next state
			const RTTI * mFirstState = status::status_default;

			static std::unique_ptr<impl> instance;
		};
		std::unique_ptr<impl> impl::instance = nullptr;

		namespace loop
		{
			void load();
			void initialize();
			void update();
			void free();
			void unload();
		} // namespace loop

	} // !nameless namespace.


	namespace system
	{
		void initialize()
		{
			impl::instance = std::make_unique<impl>();
		}

		void update()
		{
			auto & app = *impl::instance;

			// Type of the current state
			const RTTI * const curr_state =
				app.mCurrent ?
				&app.mCurrent->type() :
				status::status_default;

			//  Are we changing the state.
			if (app.mNextState != curr_state && app.mNextState != status::status_restart)
			{
				// Free old state.
				loop::free();

				// Unload old freed state.
				loop::unload();

				// TODO -> Stop Music and those kind of stuff.

				// Update state holders.
				app.mPreviousState = curr_state;
				app.mCurrent = app.mStates[app.mNextState];

				// Load new state.
				loop::load();

				// Initialize the state.
				loop::initialize();
			}

			// Are we restarting the current state ?
			if (app.mNextState != curr_state && app.mNextState == status::status_restart)
			{
				// Free old state.
				loop::free();

				// Restart is not a real state.
				app.mNextState = curr_state;

				// ReInitialize state.
				loop::initialize();
			}

			// Update Current state.
			loop::update();
		}

		

		void shutdown()
		{
			auto & app = *impl::instance;

			// Exit the current state
			app.mCurrent->free();
			app.mCurrent->unload();
			app.mStates.clear();

			for (auto & system : utils::reverse_iterate(app.mSystems))
				system.shutdown();
			impl::instance = nullptr;
		}
		
	} // namespace system

	void run()
	{
		auto & app = *impl::instance;

		while (app.mNextState != status::status_quit && !(window::main_closed()) ) //&& !(input::key_is_down(Key::shift) && input::key_is_pressed(Key::escape)) && window::exists())
		{
			// Update each system 
			for (auto & system : app.mSystems)
				system.update();

			for (auto & system : app.mSystems)
				system.frame_end();
		}
	}

	void register_core_systems()
	{
		auto & app = *impl::instance;
		app.mSystems.reserve(12); // Reserve memory to avoid reallocs

		add_system({
			window::system::initialize,
			window::system::update,
			ISystem::dummy,
			window::system::shutdown
		});

		add_system({
			resources::manager::system::initialize,
			resources::manager::system::update,
			ISystem::dummy,
			resources::manager::system::shutdown
		});

		// Hook ourselves for application state management
		add_system({
			ISystem::dummy,
			application::system::update,
			ISystem::dummy,
			ISystem::dummy
		});
	}

	void initialize_systems()
	{
		auto & app = *impl::instance;

		// Update each system 
		for (auto & system : app.mSystems)
			system.initialize();
	}

	void add_system(ISystem system)
	{
		auto & app = *impl::instance;

		app.mSystems.push_back(system);
	}

	void pause(bool new_pause)
	{
		auto & app = *impl::instance;
		if (new_pause != app.mPaused)
		{
			app.mPaused = new_pause;
			for (auto & system : app.mSystems)
				if (new_pause == true)
					system.sleep();
				else
					system.resume();
		}
	}

	bool is_paused()
	{
		auto & app = *impl::instance;
		return app.mPaused;
	}

	AppState * add_state(AppState * state)
	{
		auto & app = *impl::instance;

		app.mStates[&state->type()] = state;

		return app.mStates[&state->type()];
	}

	void set_state(const RTTI & state_type)
	{
		auto & app = *impl::instance;
		app.mNextState = &state_type;
	}

	void set_state(const RTTI* state_type)
	{
		auto & app = *impl::instance;
		app.mNextState = state_type;
	}

	const RTTI & current_state()
	{
		auto & app = *impl::instance;
		return app.mCurrent->type();
	}

	const RTTI & get_state()
	{
		auto & app = *impl::instance;
		return app.mCurrent->type();
	}

	void restart()
	{
		auto & app = *impl::instance;
		app.mNextState = status::status_restart;
	}

	void exit()
	{
		auto & app = *impl::instance;
		app.mNextState = status::status_quit;
	}

	namespace
	{
		namespace loop
		{
			void load()
			{
				auto & app = *impl::instance;
				if (app.mCurrent)
					app.mCurrent->load();
			}

			void initialize()
			{
				auto & app = *impl::instance;
				if (app.mCurrent)
					app.mCurrent->initialize();
			}

			void update()
			{
				auto & app = *impl::instance;
				if (app.mCurrent)
					app.mCurrent->update();
			}

			void free()
			{
				auto & app = *impl::instance;
				if (app.mCurrent)
					app.mCurrent->free();
			}

			void unload()
			{
				auto & app = *impl::instance;
				if (app.mCurrent)
					app.mCurrent->unload();
			}
		} // namespace loop
	} // namespace

} // !application namespace.

