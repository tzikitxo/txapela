#pragma once


#include "ISystem.hh"
#include "AppState.hh"
#include "DLL_EngineCore.hh"

namespace application
{
	namespace system
	{
		ORC_ENGINE void initialize();
		ORC_ENGINE void update();
		ORC_ENGINE void shutdown();

	} // !System namespace

	ORC_ENGINE void run();
	ORC_ENGINE void register_core_systems();
	ORC_ENGINE void initialize_systems();

	ORC_ENGINE void add_system(ISystem system);

	ORC_ENGINE void pause(bool new_pause);
	ORC_ENGINE bool is_paused();

	ORC_ENGINE AppState* add_state(AppState* state);

	ORC_ENGINE void set_state(const RTTI& state_type);

	template <typename T>
	void set_state();

	ORC_ENGINE const RTTI& get_state();
	ORC_ENGINE void restart();
	ORC_ENGINE void exit();

	template<typename T>
	void set_state()
	{
		set_state(T::TYPE);
	}

} // !application namespace.