#pragma once


#include "DLL_EngineCore.hh"

#include <functional>

template <typename T>
using fn_ptr = std::enable_if_t<std::is_function<T>::value, T *>;

namespace application
{

	class ORC_ENGINE ISystem
	{

	public:
		ISystem(std::function<void()> init, fn_ptr<void()> update, fn_ptr<void()> frame, fn_ptr<void()> shutdown);

		void initialize();
		void update();
		void frame_end();
		void shutdown();

		void sleep() noexcept;
		void resume() noexcept;
		bool is_sleep() noexcept;

		static void dummy() noexcept;

	private:
		std::function<void()> mInit = &ISystem::dummy;
		fn_ptr<void()> mUpdate = &ISystem::dummy;
		fn_ptr<void()> mFrame = &ISystem::dummy;
		fn_ptr<void()> mShutdown = &ISystem::dummy;

		bool mSleep{ false };
	};

} // !application namespace.