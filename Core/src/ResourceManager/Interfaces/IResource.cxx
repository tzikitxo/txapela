#include "IResource.hh"

#ifndef NO_GUI
#include "GUI\ImGui\imgui.h"
#endif // !NO_GUI

RTTI_IMPL(IResource, nullptr);

bool IResource::changed() const
{
	return false;
}

void IResource::to_gui() const noexcept
{
#ifndef NO_GUI
	ImGui::Text(this->mName.c_str());
	ImGui::Text(this->mFilename.mFileName.c_str());
#endif // !NO_GUI

}

bool IResource::has_extension(const char * sub_str)
{
	const bool has_that = (mFilename.mFullPath.find(sub_str) != std::string::npos);
	return has_that;
}

std::string IResource::extension() const
{
	return mFilename.mExtension;
}

const std::string & IResource::name() const
{
	return mName;
}

void IResource::path(const FilePath  path)
{
	mFilename = path;
}

void IResource::extension(std::string extension)
{
	mFilename.mExtension = extension;
}

void IResource::name(std::string name)
{
	mName = name;
}

IResource::IResource(const std::string & filename)
{
	mFilename = filename;
	mName = mFilename.mFileName;
}

IResource::IResource(const char * filename)
{
	mFilename = std::string(filename);
	mName = mFilename.mFileName;
}