#pragma once

#include "Rtti\IBase.hh"
#include "Utils\FilePath.h"
#include "DLL_EngineCore.hh"

#include <string>

class ORC_ENGINE IResource : public core::IBase
{
	RTTI_DECL;
public:
	virtual void load(const char * filename = nullptr) = 0;
	//! Frees the resource when we dont need it anymore.
	virtual void unload() = 0;

	//! Checks wheter the resource is loaded or not.
	virtual bool is_loaded() const = 0;

	//! Returns wheter the resource has changed or not.
	virtual bool changed() const;

	virtual void to_gui() const noexcept;

	//! Checks if the resource file has the given extension.
	bool has_extension(const char * sub_str);

	//! Gets the extension of the resource.
	std::string extension() const;

	//! Gets the name of the resource in const string format.
	const std::string & name() const;

	void path(const FilePath path);
	void extension(std::string extension);
	void name(std::string name);

	virtual ~IResource() {};

protected:
	IResource() {}
	//! Constructor set fills the filepath structure and gives a name to the resource, by default the name of the file.
	IResource(const std::string & filename);

	//! Constructor set fills the filepath structure and gives a name to the resource, by default the name of the file.
	IResource(const char * filename);


protected:
	std::string			mName;			//! Name the resource will have in our system.
	FilePath			mFilename;		//! Filepaht structure to manage all the path related stuff, real name, extension...
};