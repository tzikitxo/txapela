#pragma once


#include "ResourceManager\Interfaces\IResource.hh"

#include "DLL_EngineCore.hh"

#include <string>

class ORC_ENGINE Importer
{
	RTTI_DECL;
public:
	virtual IResource* load_from_file(const std::string& name) const = 0;
	virtual void destroy(void* ptr) = 0;
};

template<class T>
class TImporter : public Importer
{
protected:
	virtual T* Create()const;

public:
	void destroy(void* ptr);
	virtual IResource* load_from_file(const std::string& name) const = 0;
};

template<class T>
inline T * TImporter<T>::Create() const
{
	return new T();
}

template<class T>
inline void TImporter<T>::destroy(void * ptr)
{
	delete (reinterpret_cast<T*>(ptr));
}
