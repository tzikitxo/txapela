#include "ResourceManager.hh"

#include <fstream>
#include <memory>

#include "GFX\TextureImporter.hh"

#ifndef NO_GUI
	#include "GUI\ImGui\imgui.h"
#endif // !NO_GUI


namespace resources
{
	namespace manager
	{
		namespace
		{

			struct impl
			{

				std::map < std::string, std::map<std::string, IResource*>> mResources;		//!< Rtti (Name, resource)
				std::map<std::string, Importer*> mImporters;	//! All the importers, the key is the extension of the file.

				static std::unique_ptr<impl> instance;
			};
			std::unique_ptr<impl> impl::instance = nullptr;

			Importer * get_importer(IResource * pRes);
			Importer * get_importer(const std::string & extension);

			Importer * get_importer(IResource * pRes)
			{
				return get_importer(pRes->extension());
			}

			Importer * get_importer(const std::string & extension)
			{
				auto it = impl::instance->mImporters.find(extension);
				if (it != impl::instance->mImporters.end())
					return it->second;

				return nullptr;
			}

		}
	}
}

void resources::manager::system::initialize()
{
	resources::manager::impl::instance = std::make_unique<resources::manager::impl>();

	PNGImporter* pngImp = new PNGImporter;
	resources::manager::add_importer(".PNG", pngImp);
	resources::manager::add_importer(".png", pngImp);
	resources::manager::add_importer(".Data", pngImp);

	JPGImporter* jpgImp = new JPGImporter;
	resources::manager::add_importer(".jpg", jpgImp);
	resources::manager::add_importer(".JPG", jpgImp);

}

void resources::manager::system::update()
{
	resources::manager::to_gui();
}

void resources::manager::system::shutdown()
{
	for (auto& it : impl::instance->mResources)
	{
		
		for (auto& it_res : it.second)
		{
			if (auto * impoter = resources::manager::get_importer(it_res.second))
				impoter->destroy(it_res.second);			
		}

	}


	impl::instance->mImporters.clear();
	impl::instance->mResources.clear();
}

void resources::manager::to_gui()
{
#ifndef NO_GUI
	auto &rm = impl::instance;

	ImGui::Begin("Resource System");

	size_t count = 0;
	for (auto & s : rm->mResources)
		count += s.second.size();

	ImGui::Text("Total resources: %d", count);

	if (ImGui::CollapsingHeader("Importers"))
	{
		for (const auto & pair : rm->mImporters)
		{
			ImGui::Text("Extension: %s. Type: %s", pair.first.c_str(), pair.second->type().name());
		}
	}

	if (ImGui::CollapsingHeader("Resources"))
	{
		for (const auto & type : rm->mResources)
		{
			if (ImGui::TreeNode(type.first.c_str()))
			{
				for (const auto & res : type.second)
				{
					if (ImGui::TreeNode(res.first.c_str()))
					{
						res.second->to_gui();
						ImGui::TreePop();
					}
				}

				ImGui::TreePop();
			}
		}
	}

	ImGui::End();
	


#endif // !NO_GUI

}

void resources::manager::add_importer(std::string extension, Importer * pImporter)
{
	 impl::instance->mImporters[extension] = pImporter;
}

 void resources::manager::import(const char * filename)
{
	 std::string shader{ "" };


	 // Parse the prog file.
	 std::ifstream infile(filename);
	 if (!infile.is_open())
		 return;

	 while (!infile.eof())
	 {
		 std::getline(infile, shader);
		resources::manager::load(shader.c_str());
	 }
}

 IResource * resources::manager::load(const char * filename)
{
	 FilePath fp(filename);

	 // Get the impoter to load it.
	 Importer * importer = get_importer(fp.mExtension);

	 if (!importer)
		 return nullptr;

	 IResource * resource = importer->load_from_file(fp.mFullPath);

	 if (!resource)
		 return nullptr;


	 resources::manager::add_resource(resource->type().name(), resource);

	 return resource;
}

 IResource * resources::manager::remove(const char * rtti, const char * resourcename)
{
	 IResource * base = nullptr;
	 auto it = impl::instance->mResources.find(rtti);
	 auto it2 = it->second.find(resourcename);
	 if (it2 != it->second.end())
		 base = it2->second;

	 // Remove from the list.
	 if (base != nullptr)
		 impl::instance->mResources.erase(it);

	 return base;
}

 IResource * resources::manager::get(const char * rtti, const char * name)
{
	 auto it = impl::instance->mResources.find(rtti);
	 if (it == impl::instance->mResources.end())
		 return nullptr;

	 auto it2 = it->second.find(name);
	 if (it2 != it->second.end())
		 return it2->second;

	 // if (rtti == Texture::TYPE.GetName())
	 // 	return mResources[rtti]["default"];

	 return nullptr;
}

 IResource * resources::manager::get_by_index(const char * rtti, unsigned int index)
{
	 auto items = impl::instance->mResources[rtti];
	 unsigned int localIndex = 0;

	 for (auto & it : items)
	 {
		 if (localIndex == index)
			 return it.second;

		 localIndex++;
	 }

	 return nullptr;
}

 std::string resources::manager::get_all(const char * rtti)
 {
	 auto items = impl::instance->mResources[rtti];

	 std::string itemstring;

	 for (auto & it : items)
	 {
		 itemstring += it.first;
		 itemstring += "|";
	 }

	 std::replace(itemstring.begin(), itemstring.end(), '|', '\0');
	 return itemstring;
 }


 int resources::manager::get_index(const char * rtti, IResource * resource)
{
	 auto items = impl::instance->mResources[rtti];
	 unsigned int localIndex = 0;

	 for (auto & it : items)
	 {
		 if (it.second == resource)
			 return localIndex;

		 localIndex++;
	 }
	 return -1;
}

 void resources::manager::add_resource(const char * rtti, IResource * resource)
{
	 // Sanity Check
	 if (resource)
		impl::instance->mResources[rtti][resource->name()] = resource;
}
