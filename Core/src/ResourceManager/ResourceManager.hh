#pragma once

#include "Interfaces\Importer.hh"
#include "Interfaces\IResource.hh"

#include <map>
#include <algorithm>

#include "DLL_EngineCore.hh"

namespace resources
{
	namespace manager
	{
		namespace system
		{
			ORC_ENGINE void initialize();
			ORC_ENGINE void update();
			ORC_ENGINE void shutdown();

		} // !system namespace.


		ORC_ENGINE void to_gui();
		ORC_ENGINE void add_importer(std::string extension, Importer* import);
		ORC_ENGINE void import(const char * filename);

		ORC_ENGINE IResource* load(const char *filename);
		ORC_ENGINE IResource* remove(const char * rtti, const char * resourcename);
		ORC_ENGINE IResource* get(const char * rtti, const char* name);
		ORC_ENGINE IResource* get_by_index(const char * rtti, unsigned int inded);
		ORC_ENGINE std::string get_all(const char * rtti);
		ORC_ENGINE int get_index(const char* rtti, IResource* resource);
		ORC_ENGINE void add_resource(const char * rtti, IResource* resource);


		template<typename T>
		T* get_by_index(unsigned int inded);

		template <typename T>
		int get_index(T*resource);

		template <typename T>
		std::string get_all_items();

		template <typename T>
		T* get(const char * name);

		template <typename T>
		T* remove(const char * name);

		template <typename T>
		void add_resource(T* resource);

		template<typename T>
		inline T * get_by_index(unsigned int index)
		{
			return reinterpret_cast<T*>(resources::manager::get_by_index(T::TYPE.name(), index));
		}

		template<typename T>
		inline int get_index(T * resource)
		{
			return resources::manager::get_index(T::TYPE.name(),resource);
		}

		template<typename T>
		inline std::string get_all_items()
		{
			return resources::manager::get_all(T::TYPE.name());
		}

		template<typename T>
		inline T * get(const char * name)
		{
			return reinterpret_cast<T*>(resources::manager::get(T::TYPE.name(), name));
		}

		template<typename T>
		inline T * remove(const char * name)
		{
			return reinterpret_cast<T*>(resources::manager::remove(T::TYPE.name(), name));
		}

		template<typename T>
		inline void add_resource(T * resource)
		{
			resources::manager::add_resource(T::TYPE.name(),resource);
		}

	} // !manager namespace.

} //  !resources namespace.