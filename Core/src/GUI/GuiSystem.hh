#pragma once

#include "DLL_EngineCore.hh"

struct SDL_Window;
typedef union SDL_Event SDL_Event;

namespace GUI
{
	namespace system
	{
		ORC_ENGINE void initialize(SDL_Window* window) noexcept;
		ORC_ENGINE void shutdown() noexcept;

		ORC_ENGINE void new_frame(SDL_Window* window);
		ORC_ENGINE bool process_event(SDL_Event* event);
		
	} // namespace system

} // namespace GUI
