#pragma once

#include <Windows.h>
#include <SDL\SDL.h>

#include "GUI\ImGui\imgui.h"

#include "DLL_EngineCore.hh"

namespace window
{
	class ORC_ENGINE Window
	{
	public:
		/*!
		\brief Creates a new window and adds it to the window manager.
		This will create a new SDL window and initialize all the variables
		that the class holds to keep track of the window and reder to it, also
		creates a new context to reder to.
		*/
		Window(const char *title, int x, int y, int w,
			int h, unsigned int flags, bool first = false);

		/*!
		\brief Destroy the window, cleans all the SDL stuff. */
		~Window();

		/*! \brief Changes window title. */
		void ChangeTitle(const char * newTitle);

		/*! \brief Handle window specific events. */
		void HandleEvents(SDL_Event & e);

		/*! \brief Gets the focus and raises the window. */
		void Focus();

		/*! \brief Renders to the buffer and swap the OpenGL buffers. */
		void Render();

		HWND handler();

	public:
		// Window data
		SDL_Window* mWindow;
		SDL_GLContext mGlContext;
		int mWindowID;

		// ImGui Data.
		ImGuiContext * mGUIContext;
		bool mHasGui;

		// Window dimensions
		int mWidth;
		int mHeight;

		// Window focus
		bool mMouseFocus;
		bool mKeyboardFocus;
		bool mFullScreen;
		bool mMinimized;
		bool mShown;
		bool mClosed;
	};

	namespace system
	{
		ORC_ENGINE void initialize();
		ORC_ENGINE void update() noexcept;
		ORC_ENGINE void shutdown() noexcept;
		ORC_ENGINE void render() noexcept;
		ORC_ENGINE void handle_events(SDL_Event& evnt);

	} // !system namespace.

	int number_windows() noexcept;
	Window* window(unsigned int index) noexcept;
	ImGuiContext* gui_context(unsigned int index) noexcept;
	bool ORC_ENGINE main_closed() noexcept;
	Window* ORC_ENGINE add_window(const char *title, int x, int y, int w, int h, unsigned int flags, bool first = false);

	ORC_ENGINE void delete_window(unsigned int windowID);

} // !window namespaec.