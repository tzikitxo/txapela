#include "Window.hh"

#include "..\resource.h"

#include <Events\Events.hh>

#include <GUI\GuiSystem.hh>
#include <GUI\ImGui\imgui.h>
#include <SDL\SDL.h>
#include <SDL\SDL_syswm.h> 

// OpenGL libraries
#define GLEW_STATIC
#include "GL\glew.h"
#include "GL\wglew.h"
#include "GL\GL.h"

#include <vector>
#include <memory> // smart pointers.

namespace window
{
	namespace
	{
		class OpenGlInitializationFail : public std::exception
		{
		public:
			OpenGlInitializationFail(const char * msg) :exception(msg) {}
		};

		struct impl
		{
			std::vector<Window*> mWindows;		//!< List of currently active windows.
			SDL_GLContext glcontext;
			static std::unique_ptr<impl> instance;
		};

		std::unique_ptr<impl> impl::instance = nullptr;


	} // !nameles namespace.


	Window::Window(const char *title, int x, int y, int w, int h, unsigned int flags, bool first)
	{
		//Create window
		mWindow = SDL_CreateWindow(title, x, y, w, h, flags);
		if (mWindow)
		{
			mMouseFocus = true;
			mKeyboardFocus = true;
			mWidth = w;
			mHeight = h;

			// Create an OpenGL Context for the new window.
			if (!first)
				mGlContext = impl::instance->glcontext;

			if (!mWindow)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				SDL_DestroyWindow(mWindow);
				mWindow = nullptr;
			}
			else
			{
				// Grab window identifier.
				mWindowID = SDL_GetWindowID(mWindow);

				// Flag as opened.
				mShown = true;

				// Create the ImGui.
				mGUIContext = ImGui::CreateContext();


				// Set the icon.
				SDL_SysWMinfo wmInfo;
				SDL_VERSION(&wmInfo.version);
				SDL_GetWindowWMInfo(mWindow, &wmInfo);
				HWND hwnd = wmInfo.info.win.window;

				const HICON hicon = ::LoadIcon(
					::GetModuleHandle(0),
					MAKEINTRESOURCE(IDI_ICON1)
				);
				if (hicon)
				{
					::SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)hicon);
					::SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)(hicon));
				}


			}
		}
		else
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
		}

	}


	HWND Window::handler()
	{
		SDL_SysWMinfo wmInfo;
		SDL_VERSION(&wmInfo.version);
		SDL_GetWindowWMInfo(mWindow, &wmInfo);
		HWND hwnd = wmInfo.info.win.window;
		return hwnd;
	}

	Window::~Window()
	{
		if (mWindow)
			SDL_DestroyWindow(mWindow);

		mMouseFocus = false;
		mKeyboardFocus = false;
		mWidth = 0;
		mHeight = 0;
	}

	void Window::Render()
	{
		// Set the SDL Window binding to render.
		SDL_GL_MakeCurrent(mWindow, impl::instance->glcontext);

		// Set the ImGui Context.
		ImGui::SetCurrentContext(mGUIContext);

		// Render the draw data.
		glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
		glClearColor(0, 0, 10, 0);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui::Render();

		// Swap the buffers.
		SDL_GL_SwapWindow(mWindow);

		// Create next frame buffer.
		GUI::system::new_frame(mWindow);
	}


	void Window::HandleEvents(SDL_Event & e)
	{
		// If an event was detected for this window.
		if (e.type == SDL_WINDOWEVENT && e.window.windowID == mWindowID)
		{
			// If we are a gui window, process the events too.
			if (mGUIContext)
			{
				ImGui::SetCurrentContext(mGUIContext);
				GUI::system::process_event(&e);
			}
			switch (e.window.event)
			{
				// Window appeared.
			case SDL_WINDOWEVENT_SHOWN:
				mShown = true;
				break;

				// Window disappeared.
			case SDL_WINDOWEVENT_HIDDEN:
				mShown = false;
				break;

				// Mouse enter.
			case SDL_WINDOWEVENT_ENTER:
				mMouseFocus = true;
				break;

				// Mouse exit.
			case SDL_WINDOWEVENT_LEAVE:
				mMouseFocus = false;
				break;

				// Keyboard focus gained.
			case SDL_WINDOWEVENT_FOCUS_GAINED:
				mKeyboardFocus = true;
				break;

				// Keyboard focus lost.
			case SDL_WINDOWEVENT_FOCUS_LOST:
				mKeyboardFocus = false;
				break;

				// Window minimized.
			case SDL_WINDOWEVENT_MINIMIZED:
				mMinimized = true;
				break;

				// Window maxized.
			case SDL_WINDOWEVENT_MAXIMIZED:
				SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
				events::GMessageBox::Dispatch<events::IEvent>("Resize", &events::IEvent("Resize"));
				mMinimized = false;
				break;

				// Window restored.
			case SDL_WINDOWEVENT_RESTORED:
				mMinimized = false;
				break;

			case SDL_WINDOWEVENT_RESIZED:
				SDL_GetWindowSize(mWindow, &mWidth, &mHeight);
				events::GMessageBox::Dispatch<events::IEvent>("Resize", &events::IEvent("Resize"));
				break;

			case SDL_KEYDOWN:
			{
				break;
			}
			// Hide on close.
			case SDL_WINDOWEVENT_CLOSE:
				mClosed = true;
				SDL_HideWindow(mWindow);
				break;
			}


		}
	}

	void Window::ChangeTitle(const char * newTitle)
	{
		SDL_SetWindowTitle(mWindow, newTitle);
	}

	void Window::Focus()
	{
		// Restore window if needed.
		if (!mShown)
		{
			SDL_ShowWindow(mWindow);
		}

		// Move window forward.
		SDL_RaiseWindow(mWindow);
	}

	namespace system
	{
		void initialize()
		{
			impl::instance = std::make_unique<impl>();

			Window * aux = add_window("Txapela -- v1.5", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 870, 568, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE, true);
			impl::instance->glcontext = SDL_GL_CreateContext(aux->mWindow);
			aux->mGlContext = impl::instance->glcontext;

			GLenum err = glewInit();
			if (err != GLEW_OK)
				throw OpenGlInitializationFail(reinterpret_cast<const char *>(glewGetErrorString(err)));


			FOR_EACH(it, impl::instance->mWindows)
			{
				ImGui::SetCurrentContext((*it)->mGUIContext);
				GUI::system::initialize((*it)->mWindow);
				GUI::system::new_frame((*it)->mWindow);
			}

		}

		void update() noexcept
		{
			// Render to each window.
			render();

			FOR_EACH(it, impl::instance->mWindows)
			{
				ImGui::SetCurrentContext((*it)->mGUIContext);
				GUI::system::initialize((*it)->mWindow);
				GUI::system::new_frame((*it)->mWindow);
			}


			SDL_Event event;
			while (SDL_PollEvent(&event))
			{
				// Keyboard events for the main window.
				ImGui::SetCurrentContext(impl::instance->mWindows[0]->mGUIContext);
				GUI::system::process_event(&event);
				handle_events(event);
			}


			
			

			
		}

		void shutdown() noexcept
		{
			SDL_GL_DeleteContext(impl::instance->glcontext);
			SDL_DestroyWindow(impl::instance->mWindows[0]->mWindow);

			// Close all windows.
			FOR_EACH(it, impl::instance->mWindows)
				delete *it;

			impl::instance->mWindows.clear();			
			// SDL_DestroyWindow(window2);
		}

		void render() noexcept
		{
			// Render to each window.
			FOR_EACH(it, impl::instance->mWindows)
				(*it)->Render();

			// Swap all buffers.
		}

		void handle_events(SDL_Event & evnt)
		{
			// Handle those events for every window.
			FOR_EACH(it, impl::instance->mWindows)
				(*it)->HandleEvents(evnt);
		}

	} // !system namespace.

	int number_windows() noexcept
	{
		return impl::instance->mWindows.size();
	}

	Window * window(unsigned int index) noexcept
	{
		return index < impl::instance->mWindows.size() ? impl::instance->mWindows[index]: nullptr;
	}

	ImGuiContext * gui_context(unsigned int index) noexcept
	{
		return impl::instance->mWindows[index]->mGUIContext;
	}

	bool ORC_ENGINE main_closed() noexcept
	{
		return impl::instance->mWindows.front()->mClosed;
	}

	Window*  add_window(const char *title, int x, int y, int w, int h, unsigned int flags, bool first)
	{
		Window * temp = new Window(title, x, y, w, h, flags);
		impl::instance->mWindows.push_back(temp);
		return temp;
	}

	void delete_window(unsigned int windowID)
	{
		FOR_EACH(it, impl::instance->mWindows)
		{
			if ((*it)->mWindowID == windowID)
			{
				delete *it;
				impl::instance->mWindows.erase(it);
				return;
			}
		}
	}

} // !window namespace.