#pragma once

#include <string>
#include <Windows.h>

namespace debug
{
	namespace MBType
	{
		enum
		{
			AbortRetryIgnore = 0x00000002L,
			CancelTryContinue = 0x00000006L,
			Help = 0x00004000L,
			Ok = 0x00000000L,
			OkCancel = 0x00000001L,
			RetryCancel = 0x00000005L,
			YesNo = 0x00000004L,
			YesNoCancel = 0x00000003L
		};
	}
	namespace MBIcon
	{
		enum
		{
			Exclamation = 0x00000030L,
			Warning = 0x00000030L,
			Information = 0x00000040L,
			Asterist = 0x00000040L,
			Question = 0x00000020L,
			Stop = 0x00000010L,
			Error = 0x00000010L
		};
	}
	namespace MBRet
	{
		enum
		{
			Ok = 1,
			Cancel = 2,
			Abort = 3,
			Retry = 4,
			Ignore = 5,
			Yes = 6,
			No = 7,
			TryAgain = 10,
			Continue = 11
		};
	}

	int message_box(const char * title, const char * message, unsigned flags) noexcept
	{
		return ::MessageBox(nullptr, message, title, flags);
	}

	int message_box(const std::string & title, const std::string & message, unsigned flags) noexcept
	{
		return message_box(title.c_str(), message.c_str(), flags);
	}



} // !debug namespace.