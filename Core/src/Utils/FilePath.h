#pragma once

#include <string>

class FilePath
{
	
public:
	FilePath();

	FilePath(std::string file);
	FilePath(const char * file);
	FilePath& operator=(std::string file);
	void SetFilePath(std::string file);
	std::string GetFilePathWithNewExtension(const std::string& newExtension);


public:
	std::string mExtension;			//!< Extension of file which may be empty includes the period such as ".png", ".txt"		
	std::string mFileName;			//!< The root filename of the file without the extension or path and in lower case For "C:\Data\FileName.txt" "filename"
	std::string mFullDirectory;		//!< The path the file is locate at For"C:\Data\FileName.txt" "C:\data\"							
	std::string mFullPath;			//!< The full path including the filename For "C:\Data\FileName.txt" "c:\data\filename.txt"
};