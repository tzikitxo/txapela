#pragma once


#include <iterator> // std::begin, std::end

namespace utils
{

	//! Represents the range [begin, end). Implements the functions begin() and end() so that any range can be iterated with range for
	template <typename Iterator>
	class range
	{
	public:
		range(Iterator begin, Iterator end);

		Iterator begin() const;
		Iterator end() const;

	private:
		Iterator begin_it;
		Iterator end_it;
	};

	//! Function wrapper for making a range with automatic template parameter deduction
	template <typename Iterator>
	range<Iterator> make_range(Iterator begin, Iterator end);

	//! Wrapper of make_range with begin = rbegin(container) and end = rend(container)
	template <typename Container>
	auto reverse_iterate(Container & container);

	//! Const version of reverse_iterate
	template <typename Container>
	auto reverse_iterate(const Container & container);

}

namespace utils
{

	template <typename Iterator>
	range<Iterator>::range(Iterator b, Iterator e)
		: begin_it{ std::move(b) }
		, end_it{ std::move(e) }
	{}

	template <typename Iterator>
	Iterator range<Iterator>::begin() const
	{
		return begin_it;
	}

	template <typename Iterator>
	Iterator range<Iterator>::end() const
	{
		return end_it;
	}

	template <typename Iterator>
	range<Iterator> make_range(Iterator begin, Iterator end)
	{
		return{ begin, end };
	}

	template <typename Container>
	auto reverse_iterate(Container & container)
	{
		return make_range(std::rbegin(container), std::rend(container));
	}

	template <typename Container>
	auto reverse_iterate(const Container & container)
	{
		return make_range(std::crbegin(container), std::crend(container));
	}

}
