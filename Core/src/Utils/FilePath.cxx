#include "FilePath.h"

#include <algorithm>

namespace
{
	char convertCharacters(char d)
	{
		if (d == '/')
			return '\\';
		else
			return tolower(d);
	}

	inline void toLower(std::string &s)
	{
		std::transform(s.begin(), s.end(), s.begin(), convertCharacters);
	}
}


FilePath::FilePath()
{}



FilePath::FilePath(std::string file)
{
	SetFilePath(file);
}

FilePath::FilePath(const char * file)
{
	SetFilePath(file);
}

FilePath& FilePath::operator=(std::string file)
{
	SetFilePath(file);
	return *this;
}

void FilePath::SetFilePath(std::string file)
{
	// toLower(file);

	mFullPath = file;

	std::size_t dirEnd = file.find_last_of("\\/");
	dirEnd = (dirEnd == std::string::npos) ? 0 : dirEnd + 1;

	std::size_t fileEnd = file.find_last_of(".");
	fileEnd = (fileEnd == std::string::npos) ? file.size() : fileEnd;

	//Extension may be nothing
	mExtension = file.substr(fileEnd);
	mFileName = file.substr(dirEnd, fileEnd - dirEnd);
	mFullDirectory = file.substr(0, dirEnd);
}

std::string FilePath::GetFilePathWithNewExtension(const std::string& newExtension)
{
	return mFullDirectory + mFileName + newExtension;
}

