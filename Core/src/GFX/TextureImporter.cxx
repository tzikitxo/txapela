#include "TextureImporter.hh"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

RTTI_IMPL(JPGImporter, &Importer::TYPE);
RTTI_IMPL(PNGImporter, &Importer::TYPE);


IResource * JPGImporter::load_from_file(const std::string & filename) const
{
	unsigned int texture1;
	// texture 1
	// ---------
	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrComponents;
	// stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		printf("Failed to load texture\n");
		return nullptr;
	}
	stbi_image_free(data);

	// Create the texture.
	graphics::Texture* pTexture = this->Create();
	pTexture->mGLHandle = texture1;

	FilePath fp(filename);
	pTexture->path(fp);
	pTexture->name(fp.mFileName);

	// Check for the type based on the sub index
	std::size_t fileEnd = fp.mFileName.find_last_of("_");
	fileEnd = (fileEnd == std::string::npos) ? fp.mFileName.size() - 2 : fileEnd;

	char ctype = fp.mFileName.substr(fileEnd + 1).data()[0];

	graphics::TextureType type;
	switch (ctype)
	{
	case 's': type = graphics::TextureType::SPECULAR; break;
	case 'n': type = graphics::TextureType::NORMAL;	break;
	case 'h': type = graphics::TextureType::HEIGHT;	break;
	case 'd':
	default: type = graphics::TextureType::DIFFUSE;	break;
	}

	pTexture->mType = type;

	return pTexture;
}

IResource * PNGImporter::load_from_file(const std::string & filename) const
{
	unsigned int texture2;
	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrComponents;
	// stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char * data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 4);
	if (data)
	{
		// note that the awesomeface.png has transparency and thus an alpha channel, so make sure to tell OpenGL the data type is of GL_RGBA
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		printf("Failed to load texture\n");
		return nullptr;
	}

	stbi_image_free(data);

	// Create the texture.
	graphics::Texture* pTexture = this->Create();
	pTexture->mGLHandle = texture2;

	FilePath fp(filename);
	pTexture->path(fp);
	pTexture->name(fp.mFileName);

	// Check for the type based on the sub index
	std::size_t fileEnd = fp.mFileName.find_last_of("_");
	fileEnd = (fileEnd == std::string::npos) ? fp.mFileName.size() - 2 : fileEnd;

	char ctype = fp.mFileName.substr(fileEnd + 1).data()[0];

	graphics::TextureType type;
	switch (ctype)
	{
	case 's': type = graphics::TextureType::SPECULAR; break;
	case 'n': type = graphics::TextureType::NORMAL;	break;
	case 'h': type = graphics::TextureType::HEIGHT;	break;
	case 'd':
	default: type = graphics::TextureType::DIFFUSE;	break;
	}

	pTexture->mType = type;

	return pTexture;
}
