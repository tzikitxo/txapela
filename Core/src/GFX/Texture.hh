#pragma once


#include "ResourceManager\Interfaces\IResource.hh"

// OpenGL libraries
#define GLEW_STATIC
#include "GL\glew.h"
#include "GL\wglew.h"
#include "GL\GL.h"


#include <string>

namespace graphics
{
	enum class TextureType : unsigned int
	{
		DIFFUSE = 0u,
		SPECULAR,
		NORMAL,
		HEIGHT,

		COUNT
	};
	struct Texture : public IResource
	{
		RTTI_DECL

	public:
		Texture() {}
		Texture(const char* path);

		virtual void load(const char * filename = nullptr) override;
		virtual void unload()override;
		virtual bool is_loaded() const override;


	public:
		GLuint mGLHandle{ 0 };
		TextureType mType{ TextureType::DIFFUSE };

		unsigned char *data{ nullptr };

		unsigned int width{ 0 };
		unsigned int height{ 0 };

	};
}