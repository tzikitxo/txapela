#pragma once
#include "ResourceManager\Interfaces\Importer.hh"
#include "GFX\Texture.hh"

class JPGImporter : public TImporter < graphics::Texture >
{
	RTTI_DECL;
	IResource* load_from_file(const std::string & filename) const override;
};

class PNGImporter : public TImporter<graphics::Texture>
{
	RTTI_DECL;
	IResource* load_from_file(const std::string & filename)const override;
};