#include "Texture.hh"

namespace graphics
{
	RTTI_IMPL(Texture, &IResource::TYPE);

	Texture::Texture(const char * path)
	{
		this->path(path);
	}

	void Texture::load(const char * filename)
	{
	}

	void Texture::unload()
	{
	}

	bool Texture::is_loaded() const
	{
		return this->mGLHandle != 0;
	}

} // !graphics namespace.