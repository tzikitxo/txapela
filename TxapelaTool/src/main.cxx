#include <iostream>
#include <SDL\SDL.h>

#include "Application\Application.hh"
#include "xWingAppState.hh"


int main(int, char**)
{
	// Setup SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
	{
		printf("Error: %s\n", SDL_GetError());
		return -1;
	}

	application::system::initialize();
	application::register_core_systems();

	application::initialize_systems();

	application::add_state(new xWingApp);
	application::set_state<xWingApp>();
	application::run();
	application::system::shutdown();
		
	SDL_Quit();
	
	return 69;
}