#include "xWingAppState.hh"
#include "GUI\ImGui\imgui.h"

#include "ResourceManager\ResourceManager.hh"

#include "xWingTournament.hh"

RTTI_IMPL(xWingApp, &application::AppState::TYPE);


xWingApp::xWingApp()
{
}

void xWingApp::load()
{
	resources::manager::load("./Data/BG2.Data");
}

void xWingApp::initialize()
{
}

void xWingApp::update()
{
	tournament->update();
}

void xWingApp::free()
{
}

void xWingApp::unload()
{}
