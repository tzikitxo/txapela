#pragma once

#include "GUI\ImGui\imgui.h"

namespace gui_layout
{
	//! Default colors for the armies.
	static std::map<Army, ImVec4> gColors = { { Rebel,ImVec4(1,0,0,1) },{ Empire,ImVec4(1,1,1,1) },{ Scum,ImVec4(1,1,0,1) } };


	void main_layout() noexcept;
	void player_layout() noexcept;

} // !gui_layout.