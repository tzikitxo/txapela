#include "xWingMatch.hh"

#include "xWingPlayer.hh"
#include "xWingTournament.hh"

//! \def RTTI_IMPL Run time type identification definition, base type Icomp.
RTTI_IMPL(xWingMatch, &core::IBase::TYPE);


Json::Value xWingMatch::serialize() const
{
	Json::Value data(Json::objectValue);
	// data["Jolaskide1_ID"] = mPlayer1 ? mPlayer1->mID : -1;
	// data["Jolaskide2_ID"] = mPlayer2 ? mPlayer2->mID : -1;
	data["Jolaskide1"] = mPlayer1 ? mPlayer1->name() : "";
	data["Jolaskide2"] = mPlayer2 ? mPlayer2->name() : "";
	data["Jolaskide1Hil"] = mPlayerKills1;
	data["Jolaskide2Hil"] = mPlayerKills2;
	data["Bye"] = mIsBye;
	data["ForcedBye"] = mIsForcedBye;
	data["Bikoiztuta"] = mIsDuplicated;
	data["Irabazle"] = mWinner ? mWinner->name() : "";
	// data["Irabazle_ID"] = mWinner ? mWinner->mID : -1;

	return data;
}

void xWingMatch::unserialize(const Json::Value & data)
{
	mPlayer1 = xWingTournament::instance()->player(data["Jolaskide1"].asCString());
	mPlayer2 = xWingTournament::instance()->player(data["Jolaskide2"].asCString());
	mWinner = xWingTournament::instance()->player(data["Irabazle"].asCString());

	mPlayerKills1 = data["Jolaskide1Hil"].asInt();
	mPlayerKills2 = data["Jolaskide2Hil"].asInt();
	mIsBye = data["Bye"].asBool();
	mIsDuplicated = data["Bikoiztuta"].asBool();
	mIsForcedBye = data["ForcedBye"].asBool();

}

xWingMatch::xWingMatch(xWingPlayer * player1, xWingPlayer * player2)
	:mPlayer1(player1)
	,mPlayer2(mPlayer2)
{
	if (mPlayer1 && !player2)
		mIsBye = true;
}

void xWingMatch::winner(xWingPlayer * winner) noexcept
{
	 mWinner = winner;
}

xWingPlayer * xWingMatch::winner() const noexcept
{
	return this->mWinner;
}

xWingPlayer * xWingMatch::player1() const noexcept
{
	return this->mPlayer1;
}

void xWingMatch::player2(xWingPlayer * player) noexcept
{
	this->mPlayer2 = player;
}

xWingPlayer * xWingMatch::player2() const noexcept
{
	return this->mPlayer2;
}

bool xWingMatch::bye() const noexcept
{
	return this->mIsBye;
}

void xWingMatch::unforce_bye() noexcept
{
	this->mIsForcedBye = false;
	this->mIsBye = false;
	this->mWinner = nullptr;
}

void xWingMatch::force_bye(bool player1) noexcept
{
	this->mIsForcedBye = true;
	this->mIsBye = true;
	if (!player1)
		std::swap(this->mPlayer1, this->mPlayer2);

	this->mWinner = this->mPlayer1;
}

void xWingMatch::player1_kill(int kill)
{
	this->mPlayerKills1 = kill;
}

int xWingMatch::player1_kill() const noexcept
{
	return this->mPlayerKills1;
}

void xWingMatch::player2_kill(int kill)
{
	this->mPlayerKills2 = kill;
}

int xWingMatch::player2_kill() const noexcept
{
	return this->mPlayerKills2;
}

bool xWingMatch::duplicated() const noexcept
{
	return this->mIsDuplicated;
}

void xWingMatch::duplicated(bool dupl) noexcept
{
	this->mIsDuplicated = dupl;
}

void xWingMatch::check_duplicated() noexcept
{
	if (player1() == player2())
	{
		this->duplicated(true);
		return;
	}

	// Sanity for the byes.
	if (!this->player2())
	{
		this->duplicated(false);
		return;
	}

	auto rounds = tournament->rounds();
	// Get all rounds of the tournament.
	if (tournament->rounds().size())
	{
		FOR_EACH(itRound, rounds)
		{
			if ((*itRound)->is_single_elimination())
				continue;

			// Get All matches in the round.
			auto matches = (*itRound)->matches();
			FOR_EACH(itMatch, matches)
			{
				// If it was a bye or is ourself, ignore it.
				if (!(*itMatch)->player2() || *itMatch == this)
					continue;

				// Check for duplicated, in both orders, just in case.
				if (((*itMatch)->player1() == this->player1()
					&& (*itMatch)->player2() == this->player2())
					|| ((*itMatch)->player1() == this->player2()
						&& (*itMatch)->player1() == this->player2()))
				{
					this->duplicated(true);
					return;
				}
			}
		}
	}

	// If we found no duplicates, set it to false.
	this->duplicated(false);
}

bool xWingMatch::is_complete() const noexcept
{
	 return mIsBye || mWinner;;
}

bool xWingMatch::is_valid() const noexcept
{
	// If we dont have second player, there must be a bye.
	if (!mPlayer2 && mIsBye)
		return true;

	// Check the points and the winner is pausible.
	if ((mWinner == mPlayer1 &&  mPlayerKills1 > mPlayerKills2)
		|| (mWinner == mPlayer2 &&  mPlayerKills2 > mPlayerKills1)
		|| (mWinner && mPlayerKills1 == mPlayerKills2))
		return true;

	// Default return.
	return false;
}

bool xWingMatch::duplicated(std::vector<xWingMatch*>& matches)
{
	for(auto& itMatches : matches)
		if (itMatches->duplicated())
			return true;

	return false;
}

void xWingMatch::player1(xWingPlayer * player) noexcept
{
	this->mPlayer1 = player;
}
