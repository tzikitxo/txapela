#pragma once

#include "Rtti\IBase.hh"

#include "xWingMatch.hh"

class xWingRound : public core::IBase
{
	//! \def RTTI_DECL Run time type identification declaration.
	RTTI_DECL;

public:
	Json::Value serialize() const override;
	void unserialize(const Json::Value & data) override;

public:
	xWingRound() = default;
	xWingRound(std::vector<xWingMatch*>& matches);
	~xWingRound();

	std::vector<xWingMatch*>& matches() noexcept;
	void matches(std::vector<xWingMatch*>& matches_) noexcept;

	bool is_single_elimination() const noexcept;
	bool is_complete() const noexcept;
	bool is_valid() const noexcept;
	int size() const noexcept;

	void single_elimination(bool set) noexcept;




public: // private:
	std::vector<xWingMatch*> mMatches;
	bool mSingleElimination = false;
};