#include "xWingRound.hh"

#include "Macros.h"

//! \def RTTI_IMPL Run time type identification definition, base type Icomp.
RTTI_IMPL(xWingRound, &core::IBase::TYPE);


Json::Value xWingRound::serialize() const
{
	Json::Value data(Json::objectValue);
	Json::Value rounds(Json::arrayValue);
	if (mMatches.size())
	{
		FOR_EACH(it, mMatches)
		{
			rounds.append((*it)->serialize());
		}
	}
	data["Bikoteak"] = rounds;
	return data;
}

void xWingRound::unserialize(const Json::Value & data)
{
	// Erase all players.
	FOR_EACH(itErase, mMatches)
		delete *itErase;

	// Clear the vector.
	mMatches.clear();

	// Unserialize all matches.
	auto itPlayers = mMatches.begin();
	for (Json::Value::iterator it = data["Bikoteak"].begin(); it != data["Bikoteak"].end(); ++it)
	{
		xWingMatch * match = new xWingMatch;
		match->unserialize(*it);
		mMatches.push_back(match);
	}
}

xWingRound::xWingRound(std::vector<xWingMatch*>& matches)
	: mMatches(matches)
{
}

xWingRound::~xWingRound()
{
	// Erase all players.
	FOR_EACH(itErase, mMatches)
		delete *itErase;

	// Clear the vector.
	mMatches.clear();
}

std::vector<xWingMatch*>& xWingRound::matches() noexcept
{
	return this->mMatches;
}

void xWingRound::matches(std::vector<xWingMatch*>& matches_) noexcept
{
	this->mMatches = matches_;
}

bool xWingRound::is_single_elimination() const noexcept
{
	return this->mSingleElimination;
}

bool xWingRound::is_complete() const noexcept
{
	if (!mMatches.size())
		return false;

	FOR_EACH(itMatch, mMatches)
		if (!(*itMatch)->is_complete())
			return false;

	return true;
}

bool xWingRound::is_valid() const noexcept
{
	if (!mMatches.size())
		return false;

	FOR_EACH(itMatch, mMatches)
		if (!(*itMatch)->is_valid())
			return false;

	return true;
}

int xWingRound::size() const noexcept
{
	return static_cast<int>(this->mMatches.size());
}

void xWingRound::single_elimination(bool set) noexcept
{
	this->mSingleElimination = set;
}
