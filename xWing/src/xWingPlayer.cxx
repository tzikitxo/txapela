#include "xWingPlayer.hh"

#include "xWingTournament.hh"
#include "xWingMatch.hh"
#include "Macros.h"


//! \def RTTI_IMPL Run time type identification definition, base type Icomp.
RTTI_IMPL(xWingPlayer, &core::IBase::TYPE);

Json::Value xWingPlayer::serialize() const
{
	Json::Value data(Json::objectValue);
	data["Mota"] = TYPE.name();
	data["Taldea"] = mGroup;
	data["Izena"] = mName;
	data["Armada"] = mArmy;
	data["Dropped"] = mIsDropped;
	return data;
}

void xWingPlayer::unserialize(const Json::Value & data)
{
	mGroup = data["Taldea"].asString();
	mName = data["Izena"].asString();
	mArmy = (Army)data["Armada"].asInt();
	mIsDropped = data["Dropped"].asBool();
}

xWingPlayer::xWingPlayer(std::string & name) :mName(name)
{}

void xWingPlayer::first_round_bye(bool bye) noexcept
{
	this->mFirstRoundBye = bye;
}

bool xWingPlayer::first_round_bye() const noexcept
{
	return this->mFirstRoundBye;
}

std::string xWingPlayer::name() const noexcept
{
	return this->mName;
}

void xWingPlayer::name(std::string & name_) noexcept
{
	this->mName = name_;
}

std::string xWingPlayer::group() const noexcept
{
	return this->mGroup;
}

void xWingPlayer::group(std::string & group_) noexcept
{
	this->mGroup = group_;
}

Army xWingPlayer::army() const noexcept
{
	return this->mArmy;
}

void xWingPlayer::army(Army army_) noexcept
{
	this->mArmy = army_;
}

bool xWingPlayer::dropped() const noexcept
{
	return this->mIsDropped;
}

void xWingPlayer::dropped(bool drop) noexcept
{
	this->mIsDropped = drop;
}

std::vector<xWingMatch*> xWingPlayer::matches() const noexcept
{
	return std::vector<xWingMatch*>();
}

int xWingPlayer::score() const noexcept
{
	// Auxiliar to get the score.
	unsigned int score = 0;

	auto matches = this->matches();
	// Loop over all the matches we played and count.
	if (matches.size())
		FOR_EACH(itMatches, matches)
	{
		if ((*itMatches)->winner() == this)
			score += WIN;
		else if ((*itMatches)->bye() && !(*itMatches)->mIsForcedBye && (*itMatches)->winner() == this)
			score += BYE;
		else if ((*itMatches)->bye() && !(*itMatches)->player2())
			score += BYE;
		else
			score += LOSS;
	}

	// Return the total score.
	return score;
}

double xWingPlayer::sos() const noexcept
{
	double sos = 0.0;
	std::vector<xWingMatch*>& tempMatches = this->matches();

	// Loop over all the matches.
	if (tempMatches.size())
		FOR_EACH(itMatch, tempMatches)
	{
		if (!(*itMatch)->bye() && (*itMatch)->winner())
		{
			if ((*itMatch)->player1() == this)
			{
				sos += (*itMatch)->player2()->sos();
			}
			else
			{
				sos += (*itMatch)->player1()->sos();
			}
		}

		// Check for first round bye.
		if (this->first_round_bye() && (*itMatch)->bye() && (*itMatch) == tempMatches.front())
			sos += (tempMatches.size() - 1) * 5;


	}

	// Sanity check.
	if (tempMatches.size())
		return sos / tempMatches.size();

	return -1.0;
}

double xWingPlayer::score_average() const noexcept
{
	return (double)score() / (double)this->matches().size();
}

int xWingPlayer::wins() const noexcept
{
	int score = 0;

	auto& matches = this->matches();
	if (matches.size())
		FOR_EACH(itMatch, matches)
		{
			if ((*itMatch)->winner() == this || ((*itMatch)->bye() && !(*itMatch)->mIsForcedBye && (*itMatch)->winner() == this))
				score++;
		}

	return score;
}

int xWingPlayer::defeats() const noexcept
{
	int score = 0;

	auto& matches = this->matches();
	if (matches.size())
		FOR_EACH(itMatch, matches)
	{
		if ((*itMatch)->winner() && (*itMatch)->winner() != this)
			score++;
	}

	return score;;
}

int xWingPlayer::byes() const noexcept
{
	int score = 0;

	auto& matches = this->matches();
	if (matches.size())
		FOR_EACH(itMatch, matches)
	{
		if ((*itMatch)->bye() && !(*itMatch)->mIsForcedBye && (*itMatch)->winner() == this)
			score++;
	}

	return score;
}

int xWingPlayer::mov() const noexcept
{
	// Auxiliar to count the round we are counting on.
	size_t auxRound = 0;

	// Auxiliar to store the mov points to return.
	int mov = 0;

	auto& matches = this->matches();
	if (matches.size())
		FOR_EACH(itMatch, matches)
	{

		// Get the tournmanet points.
		int tournamentPoints = tournament->points();
		if (tournamentPoints >= 0 && tournament->scalation_points().size())
		{
			tournamentPoints = tournament->scalation_points().size() > auxRound ?
				tournament->scalation_points()[auxRound] :
				tournament->scalation_points()[tournament->scalation_points().size()];
		}

		// Did we have a bye?
		if ((*itMatch)->bye() && ((*itMatch)->mIsForcedBye && (*itMatch)->winner() == this))
		{
			mov += tournamentPoints + (tournamentPoints / 2);
			continue;
		}
		else if ((*itMatch)->bye() && !(*itMatch)->player2())
		{

			mov += tournamentPoints + (tournamentPoints / 2);
			continue;
		}
		else if (!(*itMatch)->winner() || ((*itMatch)->mIsForcedBye && (*itMatch)->winner() != this))
			continue;


		int diff = (*itMatch)->player1_kill() - (*itMatch)->player2_kill();

		if ((*itMatch)->player1() == this)
			mov += tournamentPoints + diff;
		else
			mov += tournamentPoints - diff;
	}

	return mov;
}
