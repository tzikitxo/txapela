#pragma once

#include <map>
#include <string>

enum class Language
{
	English,
	Spanish
};

static std::map<std::string, std::string> * gCurrentLanguage{ nullptr };

void change_language(Language lang);