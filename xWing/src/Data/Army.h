#pragma once

#include <map>

// FACTION ( EMPIRE, REBEL, SCUM).
enum Army
{
	Empire = 0,
	Rebel = 1,
	Scum = 2,
	COUNT = 3
};

static const std::map<Army, std::string> gArmada = { { Empire, "Empire" }
,{ Rebel, "Rebel" }
,{ Scum, "Scum" }
};


static const char * gArmadaString = { "Empire \0Rebel \0Scum " };

