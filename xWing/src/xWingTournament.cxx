#include "xWingTournament.hh"

#include "ResourceManager\ResourceManager.hh"
#include "Window\Window.hh"
#include "GFX\Texture.hh"
#include "Serialization\Serializer.hh"


#include "Data\Language.hh"

#include <chrono>
#include <algorithm>    // std::shuffle
#include <random>  
#include <Windows.h>
#include <iostream>
#include <fstream>


//! \def RTTI_IMPL Run time type identification definition, base type Icomp.
RTTI_IMPL(xWingTournament, &core::IBase::TYPE);


//! Structure to sort players in the clasification table.
namespace sorter
{
	struct Score
	{
		bool operator()(xWingPlayer* lhs, xWingPlayer* rhs)
		{
			if (lhs->score() != rhs->score())
				return lhs->score() > rhs->score();
			else if (lhs->mov() != rhs->mov())
				return lhs->mov() > rhs->mov();
			else
				return lhs->sos() > rhs->sos();
		}
	};


	struct Alphabetical
	{
		bool operator()(xWingPlayer* lhs, xWingPlayer * rhs)
		{
			return lhs->name() < rhs->name();
		}
	};

} // !sorter namespace.

Json::Value xWingTournament::serialize() const
{
	Json::Value data(Json::objectValue);
	Json::Value players(Json::arrayValue);
	Json::Value matches(Json::arrayValue);
	Json::Value rounds(Json::arrayValue);

	if (mRounds.size())
	{
		FOR_EACH(it, mRounds)
		{
			rounds.append((*it)->serialize());
		}
	}

	if (mPlayers.size())
	{
		FOR_EACH(it, mPlayers)
		{
			players.append((*it)->serialize());
		}
	}


	data["Jolaskideak"] = players;
	data["Errondak"] = rounds;
	data["JolaskideKopuru"] = mPlayersNum;
	data["Seeding"] = mSeeding;
	data["EliminazioBakarra"] = mSingleElimination;
	data["ErrondaKopuru"] = mRoundCount;
	data["Puntu"] = mPoints;
	data["Izena"] = mName;
	return data;
}

void xWingTournament::unserialize(const Json::Value & data)
{
	// Tournament Data.
	mPlayersNum = data["JolaskideKopuru"].asInt();
	mSeeding = (InitialSeeding)data["Seeding"].asInt();
	mSingleElimination = data["EliminazioBakarra"].asBool();
	mRoundCount = data["ErrondaKopuru"].asInt();
	mPoints = data["Puntu"].asInt();
	mName = data["Izena"].asString();


	// Erase all players.
	FOR_EACH(itErase, mPlayers)
		delete *itErase;

	mPlayers.clear();

	// Unserialize all players.
	for (Json::Value::iterator it = data["Jolaskideak"].begin(); it != data["Jolaskideak"].end(); ++it)
	{
		xWingPlayer * player = new xWingPlayer;
		player->unserialize(*it);
		mPlayers.push_back(player);
	}

	// Erase all rounds.
	FOR_EACH(itErase2, mRounds)
		delete *itErase2;

	mRounds.clear();

	// Unserialize all rounds.
	for (Json::Value::iterator it = data["Errondak"].begin(); it != data["Errondak"].end(); ++it)
	{
		xWingRound * round = new xWingRound;
		round->unserialize(*it);
		mRounds.push_back(round);
	}


	bCreated = true;
}

xWingTournament::xWingTournament()
{
	// Change the language to defaul, maybe move it to a *.ini.
	change_language(Language::English);

	events::GMessageBox::ConnectObj<xWingTournament, events::IEvent>("Resize", this, &xWingTournament::on_resize);
}

void xWingTournament::update() noexcept
{
	TODO("Implement this from the gui file");

	// Set to render to main gui windows.
	ImGui::SetCurrentContext(window::gui_context(0u));

	// Set the background.
	this->render_bg();

	// Set the main menu.
	this->render_main_menu();

}

void xWingTournament::points(int points) noexcept
{
	this->mPoints = points;
}

int xWingTournament::points() const noexcept
{
	return this->mPoints;
}

void xWingTournament::scalation_points(std::vector<int>& scale) noexcept
{
	this->mScalationPoints = scale;
}

const std::vector<int>& xWingTournament::scalation_points() const noexcept
{
	return this->mScalationPoints;
}

xWingRound * xWingTournament::last_round() const noexcept
{
	return mRounds.size() ? mRounds.back() : nullptr;
}

xWingRound * xWingTournament::round(int index) const noexcept
{
	return mRounds.size() <= static_cast<size_t>(index) ? mRounds[index] : nullptr;
}

const std::vector<xWingRound*>& xWingTournament::rounds() const noexcept
{
	return this->mRounds;
}

int xWingTournament::round_size() const noexcept
{
	return static_cast<int>(this->mRounds.size());
}

void xWingTournament::generate_next_round() noexcept
{
	// Check last round is complete.
	if (!last_round()->is_complete())
	{
		this->print_state(std::string{ "The round is not completed yet!" });
		return;
	}

	// If we are in single elimination and was the final, end.
	if (is_single_elimination())
	{
		if (last_round()->size() == 1)
			return;

		if (last_round()->is_valid())  // TODO -> Prompt error."At least one tournamnt result is not correct.\n"
			return;											//  "-Check if points are backwards or a draw has been set.\n"
															//  "-Draws are not allowed in single elimination rounds.\n"
															//  "--If a draw occurs, the player with initiative wins.\n"
															//  "--This can be set by going to the X-Wing menu then the view submenu and deselect 'Enter Only Points'");

															// Generate single elimination matches.
		this->generate_single_elimination(last_round()->size());
	}
	else
	{
		// Sanity for the last round.
		if (!last_round()->is_valid())
			return;

		// Generate next round.
		this->generate_round(this->round_size());
	}
}

void xWingTournament::generate_single_elimination(int cut) noexcept
{
	// Temporary holders for easier and more readable code.
	std::vector<xWingMatch*> tempMatches;
	std::vector<xWingMatch*> correctMatches;

	// If last round was already a single elimination..
	if (mTopSize != -1)
	{
		// Create one more round.
		this->mRoundCount++;

		std::vector<xWingMatch*> lastMatches = this->last_round()->matches();

		// Generate the matches.
		for (auto it = lastMatches.begin(); it != lastMatches.end(); it = it + 2)
		{
			xWingPlayer * player1 = (*it)->winner();
			xWingPlayer * player2 = (*(it + 1))->winner();
			tempMatches.push_back(new xWingMatch(player1, player2));
		}

		// This ones are already corrected.
		this->mRounds.push_back(new xWingRound(tempMatches));
	}
	else
	{
		// Create one more round.
		this->mRoundCount++;

		this->mRounds.push_back(new xWingRound(this->get_top_matches(cut, mPlayers)));

	}

}

void xWingTournament::generate_round(int index) noexcept
{
	auto fn_random_list_shuffle = [&](std::list<xWingPlayer*>&list)
	{
		std::vector<xWingPlayer*> v(list.begin(), list.end());

		// Obtain a time-based seed.
		unsigned seed = (unsigned)std::chrono::system_clock::now().time_since_epoch().count();

		// Shuffleeeeee.
		std::shuffle(v.begin(), v.end(), std::default_random_engine(seed));

		list.assign(v.begin(), v.end());
	};

	// Check we are not skipping any round.
	if (index > this->round_size())
		return;											// TODO -> maybe exeption or whatever.

														// Temporary matches.
	std::vector<xWingMatch*> tempMatches;
	tempMatches.clear();

	// Create temporary player list.
	std::list<xWingPlayer*> tempPlayers = mPlayers;

	std::list<xWingPlayer*> drop;
	drop.clear();

	for (auto & it : mPlayers)
	{
		if (it->dropped())
		{
			drop.push_back(it);
			tempPlayers.remove(it);
		}
	}

	// Byes players.
	std::list<xWingPlayer *> byes;
	byes.clear();

	// Check for first round.
	if (index == 0)
	{
		// Store the byes.
		FOR_EACH(itPlayers, tempPlayers)
		{
			if ((*itPlayers)->first_round_bye())
				byes.push_back(*itPlayers);
		}

		// Remove the byes
		FOR_EACH(itByes, byes)
			tempPlayers.remove(*itByes);

		// Check the seeding we doing for the first round.
		switch (mSeeding)
		{
		case xWingTournament::RANDOM:
		{
			this->shuffle(tempPlayers);

			// While we have players.
			while (tempPlayers.size())
			{
				xWingPlayer * player1 = tempPlayers.front();
				xWingPlayer * player2 = tempPlayers.back();

				// Remove the picked ones.
				tempPlayers.pop_front();

				// Sanity, in case there is odd number of players.
				if (player1 == player2)
					player2 = nullptr;
				else
					tempPlayers.pop_back();

				// Create the match.
				xWingMatch * match = new xWingMatch(player1, player2);
				tempMatches.push_back(match);

			}

			break;
		}
		case xWingTournament::BY_GROUP:
		{
			// Create the map to store the players based on their group.
			std::map<std::string, std::list<xWingPlayer*>> groupedPlayers;

			// Store them based on the group.
			FOR_EACH(itPlayer, tempPlayers)
				groupedPlayers[(*itPlayer)->group()].push_back(*itPlayer);

			// Temporary vector to store all the keys of the map.
			std::vector<std::string> keys;

			// Retrieve all the keys from the map, why there isnt a method that does this ??!!
			FOR_EACH(itKeys, groupedPlayers)
				keys.push_back(itKeys->first);

			// Obtain a time-based seed.
			unsigned seed = (unsigned)std::chrono::system_clock::now().time_since_epoch().count();

			// Shuffleeeeee.
			std::shuffle(keys.begin(), keys.end(), std::default_random_engine(seed));

			// Suffle all players in their list.
			FOR_EACH(it, groupedPlayers)
				fn_random_list_shuffle(it->second);


			// Auxiliar player holders for the match creation.
			xWingPlayer * player1 = nullptr;
			xWingPlayer * player2 = nullptr;



			// Create the matches based on the groups.
			while (keys.size())
			{
				unsigned int i = 0;
				while (i < keys.size())
				{
					// Sanity.
					if (!player1)
					{
						player1 = groupedPlayers[keys[i]].front();
					}
					else
					{
						player2 = groupedPlayers[keys[i]].front();

						// Add the match.
						tempMatches.push_back(new xWingMatch(player1, player2));

						// Reset the players.
						player1 = player2 = nullptr;
					}

					// Pop the first player.
					groupedPlayers[keys[i]].pop_front();

					// Erase the value if we dont have more players, else we go to next group.
					if (!groupedPlayers[keys[i]].size())
						keys.erase(std::find(keys.begin(), keys.end(), keys[i]));
					else
						i++;
				}
			}

			// Add last player, he also wants to play.
			if (player1)
				tempMatches.push_back(new xWingMatch(player1, nullptr));

			break;
		}
		case xWingTournament::ORDERED:
		default:
		{
			// While we have players.
			while (tempPlayers.size())
			{
				xWingPlayer * player1 = tempPlayers.front();
				tempPlayers.pop_front();
				xWingPlayer * player2 = nullptr;

				// Sanity check, but shouldnt be needed.
				if (tempPlayers.size())
				{
					player2 = tempPlayers.front();
					tempPlayers.pop_front();
				}

				// Create the match.
				xWingMatch * match = new xWingMatch(player1, player2);
				tempMatches.push_back(match);

			}
			break;
		}
		}

		// Add all the byes.
		FOR_EACH(itByes, byes)
			tempMatches.push_back(new xWingMatch(*itByes, nullptr));

	}
	else
	{
		tempMatches = this->matches(mPlayers);
	}

	// Create the round.
	mRounds.push_back(new xWingRound(tempMatches));


	// Set the tables.
	unsigned int index2{ 0 };
	for (auto& it : mRounds.back()->mMatches)
		it->mTable = index2++;

	TODO(" Check for stuff!! maybe move it to update");
	// TODO!! Check for stuff!! maybe move it to update.
}

bool xWingTournament::is_single_elimination() const noexcept
{
	return this->mSingleElimination;
}

void xWingTournament::begin_round() noexcept
{
	bTimer = true;
	mStarted = std::clock();
}

void xWingTournament::end_round() noexcept
{
	bTimer = false;
}

int xWingTournament::random_num() const noexcept
{
	// Do we have players?
	if (!mPlayers.size())
		return 0;

	// Get a random seed.
	srand((unsigned int)time(NULL));

	// Get a random number in the bounds of the amount of players-
	return rand() % mPlayers.size() + 1;
}

xWingPlayer * xWingTournament::player(unsigned int index) const noexcept
{
	int counter{ 0 };
	for (auto& it : mPlayers)
	{
		if (counter == index)
			return it;

		counter++;
	}
	return nullptr;
}

xWingPlayer * xWingTournament::player(const char * name) const noexcept
{
	for (auto& it : mPlayers)
	{
		if (it->name() == std::string(name))
			return it;
	}
	return nullptr;
}

void xWingTournament::sort(std::list<xWingPlayer*>& players) noexcept
{
}

std::vector<xWingMatch*> xWingTournament::matches(std::list<xWingPlayer*>& playerlist) noexcept
{
	std::vector<xWingMatch*> tempMatches;
	std::list<xWingPlayer*> tempPlayers = playerlist;
	std::list<xWingPlayer*> drop;
	drop.clear();

	// Sort the list, should be shorted.
	// std::sort(tempPlayers.begin(), tempPlayers.end(), PlayerSort);
	playerlist.sort(sorter::Score());

	xWingMatch * byeMatch = nullptr;

	for (auto & it : mPlayers)
	{
		if (it->dropped())
		{
			drop.push_back(it);
			tempPlayers.remove(it);
		}
	}

	// If odd number of players, search for next bye player.
	if (tempPlayers.size() % 2)
	{
		xWingPlayer * byePlayer = nullptr;
		size_t byesCount = 1;
		int minByes = 0;
		while (!byePlayer								// We dont have the player..
			|| byePlayer->byes() > minByes		// Has more than the min byes we loooking.
			|| (byePlayer->matches().size() && byePlayer->matches()[byePlayer->matches().size() - 1]->bye()))
		{
			// If we done a full search loop.
			if (byesCount > tempPlayers.size())
			{
				minByes++;
				byesCount = 1;
			}

			// Iterate the list, to get the last match.
			auto it = tempPlayers.end();
			for (size_t i = 0; i < byesCount; i++)
				it--;

			// Set last match, sanity aproved!!! :D
			if (it != tempPlayers.begin())
				byePlayer = *it;
			else
				byePlayer = tempPlayers.back();

			byesCount++;
		}

		// Create the bye match for later and remove the odd player.
		byeMatch = new xWingMatch(byePlayer, nullptr);
		tempPlayers.erase(std::find(tempPlayers.begin(), tempPlayers.end(), byePlayer));

	}

	// Genetare the random matched based on the xwing tournament especifications.
	tempMatches = this->generate_random(tempPlayers);


	// Add the bye match if we got one.
	if (byeMatch)
		tempMatches.push_back(byeMatch);

	return tempMatches;
}

std::vector<xWingMatch*> xWingTournament::generate_random(std::list<xWingPlayer*>& players) noexcept
{
	// Create the map which keys will be the values for each match making.
	std::map<int, std::list<xWingPlayer*>> pointsMapping;

	// Add each player to their corresponding point group.
	FOR_EACH(itPlayers, players)
		pointsMapping[(*itPlayers)->score()].push_back(*itPlayers);

	// Get the first point group.
	int firstGroup = pointsMapping.rbegin()->first;

	// Call the recursive function.
	std::vector<xWingMatch*> matches = this->resolve_point_group(nullptr, pointsMapping, pointsMapping[firstGroup]);

	// After recursivity the matches are done.
	return matches;
}

std::vector<xWingMatch*> xWingTournament::resolve_point_group(xWingPlayer * carriedOver, std::map<int, std::list<xWingPlayer*>>& pointGroup, std::list<xWingPlayer*>& players) noexcept
{
	// this->ShufflePlayerList(players);

	// Holder for the carried over player, in case the group is odd.
	xWingPlayer* newCarriedPlayer = nullptr;
	// auto carriedIndex = players.end(); 

	// Check if is gonna be carried someone for the next round.
	bool isCarried = false;
	if (carriedOver)
		isCarried = players.size() % 2 == 0;
	else
		isCarried = players.size() % 2 == 1;

	// Sanity, if there are no players.
	if (players.size() == 0)
		isCarried = false;

	// Recursivity infnite loop, will return and exit when fully done.
	while (true)
	{
		std::list<xWingPlayer*> tempPlayers = players;

		// Remove the carried over one.
		if (isCarried && players.size())
		{
			// carriedIndex--;
			newCarriedPlayer = players.back();
			tempPlayers.remove(newCarriedPlayer);
			// tempPlayers.erase(std::find(players.begin(), players.end(),players.back()));
		}

		std::vector<xWingMatch*> returnedMatches = this->get_random_matches(carriedOver, tempPlayers);

		// If the list was good or if there was no carry over players that can change things up.
		if (!isCarried || (players.size() && newCarriedPlayer == players.front())
			|| !xWingMatch::duplicated(returnedMatches))
		{

			auto itMap = pointGroup.end();
			itMap--;

			// Sanity.
			if (players.size())
			{
				itMap = pointGroup.find(players.front()->score());
				while (itMap != pointGroup.end() && itMap->second == players)
					itMap--;
			}
			else
			{
				itMap = pointGroup.find(newCarriedPlayer->score());

				// Sanity.
				if (itMap != pointGroup.end())
					itMap--;
			}



			// If there are not more groups, we are done.
			if (itMap == pointGroup.end())
				return returnedMatches;
			else
			{
				std::vector<xWingMatch*> nextPointGroupPlayers = this->resolve_point_group(newCarriedPlayer, pointGroup, itMap->second);

				// Check if that one was the last one.
				if (!isCarried || (players.size() && newCarriedPlayer == players.back()) || !xWingMatch::duplicated(nextPointGroupPlayers))
				{
					FOR_EACH(itMatch, nextPointGroupPlayers)
						returnedMatches.push_back(*itMatch);

					return returnedMatches;
				}
			}
		}
		else
		{
			for (auto& it : returnedMatches)
				delete it;

		}

	}
}

std::vector<xWingMatch*> xWingTournament::get_random_matches(xWingPlayer * carriedOver, std::list<xWingPlayer*>& players) noexcept
{
	// Helper function to find random player in the list.
	auto fn_find_random = [&](std::list<xWingPlayer*> list)->xWingPlayer*
	{
		// Get a random seed.
		srand((unsigned int)time(NULL));
		int index = rand() % list.size();

		index = index == 0 ? 1 : index;

		int count = 0;
		for (auto& it : list)
		{
			if (count == index)
				return it;

			count++;
		}
		return nullptr;
	};

	// Holder for the matches we gonna return.
	std::vector<xWingMatch*> tempMatches;

	// Sanity, we are out of players.
	if (!players.size())
		return tempMatches;

	// Temporary for the recursivity.
	std::vector<xWingMatch*> subMatches;
	xWingMatch * currentMatch = new xWingMatch();

	// If we carried a player, it must be assigned the first one.
	if (carriedOver)
	{
		currentMatch->player1(carriedOver);

		// Loop over the rest of the players of the group.
		FOR_EACH(itPlayer, players)
		{


			currentMatch->player2(fn_find_random(players));
			currentMatch->check_duplicated();

			// Continue if is not duplicated or was the last chance.
			if (!currentMatch->duplicated() || itPlayer == players.end())
			{
				std::list<xWingPlayer*> nextPlayers = players;
				nextPlayers.remove(currentMatch->player2());

				// Get the rest of the matches.
				subMatches = this->get_random_matches(nullptr, nextPlayers);

				// If those submatches are clean, keep on going.
				if (!xWingMatch::duplicated(subMatches))
				{
					tempMatches.push_back(currentMatch);
					FOR_EACH(itMatch, subMatches)
						tempMatches.push_back(*itMatch);

					return tempMatches;
				}
			}
		}
	}
	else
	{
		// Set the first one as control, because I can :D
		currentMatch->player1(players.front());

		// Loop over the other players.
		FOR_EACH(itPlayer, players)
		{

			currentMatch->player2(fn_find_random(players));
			currentMatch->duplicated();

			// Continue if it was not duplicated or it was not the last chance.
			if (!currentMatch->duplicated() || itPlayer == players.end())
			{
				std::list<xWingPlayer*> nextPlayers = players;
				nextPlayers.remove(currentMatch->player2());
				nextPlayers.remove(currentMatch->player1());

				// Get the rest of the matches.
				subMatches = this->get_random_matches(nullptr, nextPlayers);

				// If those submatches are clean, keep on going.
				if (!xWingMatch::duplicated(subMatches))
				{
					tempMatches.push_back(currentMatch);
					FOR_EACH(itMatch, subMatches)
						tempMatches.push_back(*itMatch);

					return tempMatches;
				}
			}
		}

	}

	// This is the base case, we havent found a perfect matching for the round.
	tempMatches.push_back(currentMatch);
	FOR_EACH(itMatch, subMatches)
		tempMatches.push_back(*itMatch);

	// Exit.
	return tempMatches;
}

std::vector<xWingMatch*> xWingTournament::get_top_matches(int top_size, std::list<xWingPlayer*>& player_list) noexcept
{
	std::vector<xWingMatch*> top_matches;

	if (player_list.size() < (unsigned int)top_size)
		return top_matches;

	auto fn_top_8 = [&]
	{

		top_matches.push_back(new xWingMatch(mTopPlayers[0], mTopPlayers[7]));
		top_matches.push_back(new xWingMatch(mTopPlayers[3], mTopPlayers[4]));
		top_matches.push_back(new xWingMatch(mTopPlayers[2], mTopPlayers[5]));
		top_matches.push_back(new xWingMatch(mTopPlayers[1], mTopPlayers[6]));
	};

	auto fn_top_16 = [&]
	{


		top_matches.push_back(new xWingMatch(mTopPlayers[0], mTopPlayers[15]));
		top_matches.push_back(new xWingMatch(mTopPlayers[7], mTopPlayers[8]));
		top_matches.push_back(new xWingMatch(mTopPlayers[3], mTopPlayers[12]));
		top_matches.push_back(new xWingMatch(mTopPlayers[4], mTopPlayers[11]));
		top_matches.push_back(new xWingMatch(mTopPlayers[1], mTopPlayers[14]));
		top_matches.push_back(new xWingMatch(mTopPlayers[6], mTopPlayers[9]));
		top_matches.push_back(new xWingMatch(mTopPlayers[2], mTopPlayers[13]));
		top_matches.push_back(new xWingMatch(mTopPlayers[5], mTopPlayers[10]));
	};


	auto fn_top_32 = [&]
	{

		top_matches.push_back(new xWingMatch(mTopPlayers[0], mTopPlayers[31]));
		top_matches.push_back(new xWingMatch(mTopPlayers[15], mTopPlayers[16]));
		top_matches.push_back(new xWingMatch(mTopPlayers[8], mTopPlayers[23]));
		top_matches.push_back(new xWingMatch(mTopPlayers[7], mTopPlayers[24]));
		top_matches.push_back(new xWingMatch(mTopPlayers[3], mTopPlayers[28]));
		top_matches.push_back(new xWingMatch(mTopPlayers[12], mTopPlayers[19]));
		top_matches.push_back(new xWingMatch(mTopPlayers[11], mTopPlayers[20]));
		top_matches.push_back(new xWingMatch(mTopPlayers[4], mTopPlayers[27]));

		top_matches.push_back(new xWingMatch(mTopPlayers[1], mTopPlayers[31]));
		top_matches.push_back(new xWingMatch(mTopPlayers[14], mTopPlayers[17]));
		top_matches.push_back(new xWingMatch(mTopPlayers[9], mTopPlayers[22]));
		top_matches.push_back(new xWingMatch(mTopPlayers[6], mTopPlayers[25]));
		top_matches.push_back(new xWingMatch(mTopPlayers[2], mTopPlayers[29]));
		top_matches.push_back(new xWingMatch(mTopPlayers[13], mTopPlayers[18]));
		top_matches.push_back(new xWingMatch(mTopPlayers[10], mTopPlayers[21]));
		top_matches.push_back(new xWingMatch(mTopPlayers[6], mTopPlayers[27]));


	};


	// Set the size of the top.
	mTopSize = top_size;

	// Get the top players.
	auto it = player_list.begin();
	// Get the first players.
	for (int index = 0; index < top_size; index++)
	{
		this->mTopPlayers.push_back(*it);
		it++;
	}

	switch (top_size)
	{
	default:
	case 8:	 fn_top_8(); break;
	case 16: fn_top_16(); break;
	case 32: fn_top_32(); break;

	}


	return top_matches;
}

void xWingTournament::shuffle(std::list<xWingPlayer*>& list) const noexcept
{
	// Push the items into a vector with random acces iterator.
	std::vector< xWingPlayer*> temp;

	FOR_EACH(it, list)
		temp.push_back(*it);

	// Shuffle.

	// Obtain a time-based seed:
	unsigned seed = (unsigned)std::chrono::system_clock::now().time_since_epoch().count();

	// Shuffleeeeee
	std::shuffle(temp.begin(), temp.end(), std::default_random_engine(seed));

	// Clear the list.
	list.clear();

	// Push the values back to the list.
	FOR_EACH(itV, temp)
		list.push_back(*itV);
}

void xWingTournament::print_state(std::string & state) noexcept
{
	mState = state;
	mStateCount = 0;
}

void xWingTournament::swap(xWingPlayer * player1, xWingPlayer * player2) const noexcept
{
	// not necesary
}

void xWingTournament::delete_last_round() noexcept
{
	delete mRounds.back();
	mRounds.pop_back();
}

void xWingTournament::clear() noexcept
{
	while (mPlayers.size())
	{
		delete mPlayers.back();
		mPlayers.pop_back();
	}

	while (mRounds.size())
	{
		delete mRounds.back();
		mRounds.pop_back();
	}

	mScalationPoints.clear();
	mSeeding = InitialSeeding::RANDOM;
	mName = "Default Tournament";
	mSingleElimination = false;

	bCreated = false;
	bTimer = false;

	mSelected = nullptr;
	mMatchSelected = nullptr;

	move0 = false;
	move1 = false;
}

void xWingTournament::on_resize(events::IEvent * evnt)
{
	move0 = false;
	move1 = false;
}

void xWingTournament::dump_matches(const char * filename)
{
	std::fstream file(filename, std::fstream::out);
	if (file.is_open())
	{
		std::list<xWingPlayer*> to_sort = mPlayers;
		to_sort.sort(sorter::Alphabetical());

		file << "Created by Txapela Tournament Manager.\n\n\n";
		file << "Tournament: " << this->mName;
		file << "\nRound: " << this->round_size();
		file << "\n-------------------------------------\n\n";
		file << "Tables:\n";

		for (auto& it : to_sort)
		{
			file << "  " << it->matches().back()->mTable << "  : " << it->name() << "  VS  "
				<< ((it->matches().back()->player2() == it) ? it->matches().back()->player1()->name() : it->matches().back()->player2()->name())
				<< "\n";
		}

		file.close();

	}
}

std::pair<std::string, int> xWingTournament::gui_printable_players() const noexcept
{
	std::string itemstring;
	int counter = 0;

	for (auto & it : mPlayers)
	{
		// if (it->mIsDropped)
		// 	continue;
		counter++;
		itemstring += it->name();
		itemstring += "|";
	}

	std::replace(itemstring.begin(), itemstring.end(), '|', '\0');
	return{ itemstring,counter };
}

void xWingTournament::render_bg() const noexcept
{
	// Create the background holder.
	ImGui::SetNextWindowPos(ImVec2(-10, -10));
	ImGui::SetNextWindowSize(ImVec2((float)window::window(0u)->mWidth + 20, (float)window::window(0u)->mHeight + 20));
	ImGui::Begin("bg", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoScrollbar);
	ImTextureID tex_id = (void*)resources::manager::get<graphics::Texture>("BG2")->mGLHandle;  
	ImGui::Image(tex_id, ImVec2((float)window::window(0u)->mWidth + 20, (float)window::window(0u)->mHeight + 20), ImVec2(0, 0), ImVec2(1, -1));
	ImGui::End();
}

void xWingTournament::render_main_menu() noexcept
{
	// Create the menu bar.
	ImGui::BeginMainMenuBar();
	if (ImGui::BeginMenu("File"))
	{
		// Create a new tournament.
		if (ImGui::MenuItem("New"))
			bNewTour = true;

		// If there is already a tournament, because we got players.
		if (mPlayers.size())
		{
			if (ImGui::MenuItem("Save"))
			{				
				Json::Value data = this->serialize();

				char filename[MAX_PATH];

				OPENFILENAME ofn;
				ZeroMemory(&filename, sizeof(filename));
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
				ofn.lpstrFilter = "Xwing Txapela file\0*.XTT";
				ofn.lpstrFile = filename;
				ofn.nMaxFile = MAX_PATH;
				ofn.lpstrDefExt = ".XTT";
				ofn.lpstrTitle = "Select a file to save";
				ofn.Flags = OFN_DONTADDTORECENT | OFN_HIDEREADONLY;



				if (!GetSaveFileNameA(&ofn))
				{
					// All this stuff below is to tell you exactly how you messed up above. 
					// Once you've got that fixed, you can often (not always!) reduce it to a 'user cancelled' assumption.
					switch (CommDlgExtendedError())
					{
					case CDERR_DIALOGFAILURE: std::cout << "CDERR_DIALOGFAILURE\n";   break;
					case CDERR_FINDRESFAILURE: std::cout << "CDERR_FINDRESFAILURE\n";  break;
					case CDERR_INITIALIZATION: std::cout << "CDERR_INITIALIZATION\n";  break;
					case CDERR_LOADRESFAILURE: std::cout << "CDERR_LOADRESFAILURE\n";  break;
					case CDERR_LOADSTRFAILURE: std::cout << "CDERR_LOADSTRFAILURE\n";  break;
					case CDERR_LOCKRESFAILURE: std::cout << "CDERR_LOCKRESFAILURE\n";  break;
					case CDERR_MEMALLOCFAILURE: std::cout << "CDERR_MEMALLOCFAILURE\n"; break;
					case CDERR_MEMLOCKFAILURE: std::cout << "CDERR_MEMLOCKFAILURE\n";  break;
					case CDERR_NOHINSTANCE: std::cout << "CDERR_NOHINSTANCE\n";     break;
					case CDERR_NOHOOK: std::cout << "CDERR_NOHOOK\n";          break;
					case CDERR_NOTEMPLATE: std::cout << "CDERR_NOTEMPLATE\n";      break;
					case CDERR_STRUCTSIZE: std::cout << "CDERR_STRUCTSIZE\n";      break;
					case FNERR_BUFFERTOOSMALL: std::cout << "FNERR_BUFFERTOOSMALL\n";  break;
					case FNERR_INVALIDFILENAME: std::cout << "FNERR_INVALIDFILENAME\n"; break;
					case FNERR_SUBCLASSFAILURE: std::cout << "FNERR_SUBCLASSFAILURE\n"; break;
					default: std::cout << "You cancelled.\n";
					}


				}

				serializer::save_file(filename, data);
				
			}

			if (ImGui::MenuItem("Load"))
			{

				char filename[MAX_PATH];

				OPENFILENAME ofn;
				ZeroMemory(&filename, sizeof(filename));
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
				ofn.lpstrFilter = "Xwing Txapela file\0*.XTT";
				ofn.lpstrFile = filename;
				ofn.nMaxFile = MAX_PATH;
				ofn.lpstrDefExt = ".XTT";
				ofn.lpstrTitle = "Select a file to Load";
				ofn.Flags = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

				if (!GetOpenFileNameA(&ofn))
				{
					// All this stuff below is to tell you exactly how you messed up above. 
					// Once you've got that fixed, you can often (not always!) reduce it to a 'user cancelled' assumption.
					switch (CommDlgExtendedError())
					{
					case CDERR_DIALOGFAILURE: std::cout << "CDERR_DIALOGFAILURE\n";   break;
					case CDERR_FINDRESFAILURE: std::cout << "CDERR_FINDRESFAILURE\n";  break;
					case CDERR_INITIALIZATION: std::cout << "CDERR_INITIALIZATION\n";  break;
					case CDERR_LOADRESFAILURE: std::cout << "CDERR_LOADRESFAILURE\n";  break;
					case CDERR_LOADSTRFAILURE: std::cout << "CDERR_LOADSTRFAILURE\n";  break;
					case CDERR_LOCKRESFAILURE: std::cout << "CDERR_LOCKRESFAILURE\n";  break;
					case CDERR_MEMALLOCFAILURE: std::cout << "CDERR_MEMALLOCFAILURE\n"; break;
					case CDERR_MEMLOCKFAILURE: std::cout << "CDERR_MEMLOCKFAILURE\n";  break;
					case CDERR_NOHINSTANCE: std::cout << "CDERR_NOHINSTANCE\n";     break;
					case CDERR_NOHOOK: std::cout << "CDERR_NOHOOK\n";          break;
					case CDERR_NOTEMPLATE: std::cout << "CDERR_NOTEMPLATE\n";      break;
					case CDERR_STRUCTSIZE: std::cout << "CDERR_STRUCTSIZE\n";      break;
					case FNERR_BUFFERTOOSMALL: std::cout << "FNERR_BUFFERTOOSMALL\n";  break;
					case FNERR_INVALIDFILENAME: std::cout << "FNERR_INVALIDFILENAME\n"; break;
					case FNERR_SUBCLASSFAILURE: std::cout << "FNERR_SUBCLASSFAILURE\n"; break;
					default: std::cout << "You cancelled.\n";
					}

				}

				
				Json::Value data = serializer::load_file(filename);
				this->unserialize(data);
			}

			ImGui::Separator();

			ImGui::MenuItem("Export", nullptr, false, false);
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("We are working on it... Be patient!");

			ImGui::Separator();

			// Item for the secondary window.
			if (window::number_windows() > 1 && window::window(1u))
			{
				if (window::window(1u)->mClosed)
				{
					if (ImGui::MenuItem("Show Public Window"))
					{
						window::window(1u)->mClosed = false;
						SDL_ShowWindow(window::window(1u)->mWindow);
					}
				}
				else
				{
					if (ImGui::MenuItem("Hide Public Window"))
					{
						window::window(1u)->mClosed = true;
						SDL_HideWindow(window::window(1u)->mWindow);
					}
				}
			}

		}
		else
		{
			ImGui::MenuItem("Save", nullptr, false, false);
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Nothing to save.");

			if (ImGui::MenuItem("Load"))
			{
				char filename[MAX_PATH];

				OPENFILENAME ofn;
				ZeroMemory(&filename, sizeof(filename));
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(ofn);
				ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
				ofn.lpstrFilter = "Xwing Txapela file\0*.XTT";
				ofn.lpstrFile = filename;
				ofn.nMaxFile = MAX_PATH;
				ofn.lpstrDefExt = ".XTT";
				ofn.lpstrTitle = "Select a file to Load";
				ofn.Flags = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

				if (!GetOpenFileNameA(&ofn))
				{
					// All this stuff below is to tell you exactly how you messed up above. 
					// Once you've got that fixed, you can often (not always!) reduce it to a 'user cancelled' assumption.
					switch (CommDlgExtendedError())
					{
					case CDERR_DIALOGFAILURE: std::cout << "CDERR_DIALOGFAILURE\n";   break;
					case CDERR_FINDRESFAILURE: std::cout << "CDERR_FINDRESFAILURE\n";  break;
					case CDERR_INITIALIZATION: std::cout << "CDERR_INITIALIZATION\n";  break;
					case CDERR_LOADRESFAILURE: std::cout << "CDERR_LOADRESFAILURE\n";  break;
					case CDERR_LOADSTRFAILURE: std::cout << "CDERR_LOADSTRFAILURE\n";  break;
					case CDERR_LOCKRESFAILURE: std::cout << "CDERR_LOCKRESFAILURE\n";  break;
					case CDERR_MEMALLOCFAILURE: std::cout << "CDERR_MEMALLOCFAILURE\n"; break;
					case CDERR_MEMLOCKFAILURE: std::cout << "CDERR_MEMLOCKFAILURE\n";  break;
					case CDERR_NOHINSTANCE: std::cout << "CDERR_NOHINSTANCE\n";     break;
					case CDERR_NOHOOK: std::cout << "CDERR_NOHOOK\n";          break;
					case CDERR_NOTEMPLATE: std::cout << "CDERR_NOTEMPLATE\n";      break;
					case CDERR_STRUCTSIZE: std::cout << "CDERR_STRUCTSIZE\n";      break;
					case FNERR_BUFFERTOOSMALL: std::cout << "FNERR_BUFFERTOOSMALL\n";  break;
					case FNERR_INVALIDFILENAME: std::cout << "FNERR_INVALIDFILENAME\n"; break;
					case FNERR_SUBCLASSFAILURE: std::cout << "FNERR_SUBCLASSFAILURE\n"; break;
					default: std::cout << "You cancelled.\n";
					}

				}

				Json::Value data = serializer::load_file(filename);
				this->unserialize(data);
			}

			ImGui::Separator();

			ImGui::MenuItem("Export", nullptr, false, false);
			if (ImGui::IsItemHovered())
				ImGui::SetTooltip("Nothing to export.");

			ImGui::Separator();

			// Item for the secondary window.
			if (window::number_windows() > 1 && window::window(1u))
			{
				if (window::window(1u)->mClosed)
				{
					if (ImGui::MenuItem("Show Public Window"))
					{
						window::window(1u)->mClosed = false;
						SDL_ShowWindow(window::window(1u)->mWindow);
					}
				}
				else
				{
					if (ImGui::MenuItem("Hide Public Window"))
					{
						window::window(1u)->mClosed = true;
						SDL_HideWindow(window::window(1u)->mWindow);
					}
				}
			}
		}

		ImGui::EndMenu();
	}

	if (ImGui::BeginMenu("About"))
	{
		//		if (ImGui::MenuItem("Donate"))
		//		{
		//#ifdef _DEBUG
		//			ShellExecute(0, 0, "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=T3LS9H28L5EV2", 0, 0, SW_SHOW);
		//#else
		//			ShellExecute(0, 0, L"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=T3LS9H28L5EV2", 0, 0, SW_SHOW);
		//#endif
		//		}
		//
		ImGui::MenuItem("Created by TzikitxoV", nullptr, false, false);
		ImGui::MenuItem("tzikitxo@hotmail.com", nullptr, false, false);
		ImGui::MenuItem("V 0.1", nullptr, false, false);
		ImGui::EndMenu();
	}


	ImGui::EndMainMenuBar();
}

