#pragma once

#include "Rtti\IBase.hh"
#include "Events\Events.hh"

#include "xWingPlayer.hh"
#include "xWingMatch.hh"
#include "xWingRound.hh"

#include <list>
#include <ctime>

#define tournament xWingTournament::instance()

class xWingTournament : public core::IBase
{

	//! \def RTTI_DECL Run time type identification declaration.
	RTTI_DECL;

public:
	enum InitialSeeding
	{
		RANDOM,
		BY_GROUP,
		ORDERED,

		COUNT
	};

public:
	static xWingTournament* instance()
	{
		static xWingTournament* singleton = new xWingTournament();
		return singleton;
	}

	Json::Value serialize() const override;
	void unserialize(const Json::Value & data) override;

private:
	xWingTournament();
	xWingTournament(const xWingTournament &) = delete;
	const xWingTournament & operator=(const xWingTournament &) = delete;

public:

	void update() noexcept;

	void points(int points) noexcept;
	int points() const noexcept;

	void scalation_points(std::vector<int>& scale) noexcept;
	const std::vector<int>& scalation_points() const noexcept;

	xWingRound * last_round()const noexcept;
	xWingRound* round(int index) const noexcept;
	const std::vector<xWingRound*>& rounds() const noexcept;
	int round_size()const noexcept;

	void generate_next_round() noexcept;
	void generate_single_elimination(int cut = 0) noexcept;
	void generate_round(int index) noexcept;

	bool is_single_elimination() const noexcept;

	void begin_round() noexcept;
	void end_round() noexcept;

	int random_num() const noexcept;

	xWingPlayer* player(unsigned int index) const noexcept;
	xWingPlayer* player(const char * name) const noexcept;

private:

	DEPRECATED void swap(xWingPlayer* player1, xWingPlayer* player2) const noexcept;
	DEPRECATED void sort(std::list<xWingPlayer*>& players) noexcept;

	std::vector<xWingMatch*> matches(std::list<xWingPlayer*>& playerlist) noexcept;
	std::vector<xWingMatch*> generate_random(std::list<xWingPlayer*>& playerList) noexcept;
	std::vector<xWingMatch*> resolve_point_group(xWingPlayer* carriedOver, std::map<int, std::list<xWingPlayer*>>& pointGroup, std::list<xWingPlayer*>& players) noexcept;
	std::vector<xWingMatch*> get_random_matches(xWingPlayer* carriedOver, std::list<xWingPlayer*>& players) noexcept;
	std::vector<xWingMatch*> get_top_matches(int top_size, std::list<xWingPlayer*>& player_list) noexcept;

	void shuffle(std::list<xWingPlayer*>& list) const noexcept;
	void print_state(std::string& state) noexcept;
	

	void delete_last_round() noexcept;
	void clear() noexcept;
	void on_resize(events::IEvent* evnt);

	void dump_matches(const char * filename);
	std::pair<std::string, int> gui_printable_players() const noexcept;

	// Layout functions.
private:

	void render_bg() const noexcept;
	void render_main_menu() noexcept;

public: // private:
	std::list<xWingPlayer*> mPlayers;
	std::vector<xWingRound*> mRounds;
	std::vector<int> mScalationPoints;
	std::vector<xWingPlayer*> mTopPlayers;

	InitialSeeding mSeeding = InitialSeeding::RANDOM;
	std::string mName = "Default Tournament";

	int mPoints = 100;
	bool mSingleElimination = false;

private:
	bool bCreated = false;
	bool bNewTour = false;
	bool bTimer = false;
	bool bSwap = false;
	bool bSwap2 = false;

	bool bStartTop = false;

	bool bShowMainTop = false;
	bool bShowPlayerTop = false;

	bool bShowMovSos = false;

	float mClasificationSize = 0.6f;
	float mMatchesSize = 0.6f;
	float mClockSize = 4.f;

	int mClampNamesSize = -1;
	std::clock_t mStarted;
	int mRoundMaxTime = 75 * 60;
	int mPlayersNum = 0;
	int mRoundCount = 0;
	int mNormalRoundCount = 0;
	std::string mState = "";
	int mStateCount = 0;

	int mTopSize = -1;

	int topSelected{ -1 };

	int swapISelected{ -1 };
	xWingMatch* swapSelected{ nullptr };

	xWingPlayer* mSelected = nullptr;
	int mISelected = -1;

	xWingMatch * mMatchSelected = nullptr;
	int mIMatchSelected = -1;

	bool move1 = false;
	bool move0 = false;

};

