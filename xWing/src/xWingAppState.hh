#pragma once

#include "Application\AppState.hh"


//! In editor state. Contains the current game
class  xWingApp : public application::AppState
{
public:
	RTTI_DECL;

	xWingApp();

	void load() override;
	void initialize() override;
	void update() override;
	void free() override;
	void unload() override;
};
