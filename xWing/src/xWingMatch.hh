#pragma once

#include "Rtti\IBase.hh"
#include <vector>

//! Score when winning.
#define WIN 1

//! Score when having a bye.
#define BYE 1

//! Score when loosing.
#define LOSS 0

//! Forward definition of the player.
class xWingPlayer;

class xWingMatch : public core::IBase
{
	//! \def RTTI_DECL Run time type identification declaration.
	RTTI_DECL;

public:
	Json::Value serialize() const override;
	void unserialize(const Json::Value & data) override;

public:
	xWingMatch(xWingPlayer * player1 = nullptr, xWingPlayer * player2 = nullptr);

	void winner(xWingPlayer* winner) noexcept;
	xWingPlayer* winner() const noexcept;

	void player1(xWingPlayer* player) noexcept;
	xWingPlayer* player1() const noexcept;

	void player2(xWingPlayer* player) noexcept;
	xWingPlayer* player2() const noexcept;

	bool bye() const noexcept;

	void unforce_bye() noexcept;
	void force_bye(bool player1= true) noexcept;

	void player1_kill(int kill);
	int player1_kill() const noexcept;

	void player2_kill(int kill);
	int player2_kill() const noexcept;

	bool duplicated() const noexcept;
	void duplicated(bool dupl) noexcept;
	void check_duplicated() noexcept;

	bool is_complete() const noexcept;
	bool is_valid() const noexcept;

	static bool duplicated(std::vector<xWingMatch*>& matches);

public: //private:
	xWingPlayer * mPlayer1{ nullptr };
	xWingPlayer * mPlayer2{ nullptr };
	xWingPlayer * mWinner{ nullptr };

	bool mIsBye{ false };

	int mPlayerKills1 {0};
	int mPlayerKills2 {0};

	unsigned int mTable{ 0u };
	bool mIsDuplicated{ false };

	bool mIsForcedBye{ false };


};
