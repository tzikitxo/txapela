#pragma once

#include "Rtti\IBase.hh"	
#include "Data\Army.h"

class xWingMatch;


class xWingPlayer : public core::IBase
{
	//! \def RTTI_DECL Run time type identification declaration.
	RTTI_DECL;

public:
	// Serialization.
	virtual Json::Value serialize() const override;
	virtual void unserialize(const Json::Value & data) override;


public:
	xWingPlayer() = default;
	xWingPlayer(std::string& name);

	void first_round_bye(bool bye) noexcept;
	bool first_round_bye() const noexcept;
	
	std::string name() const noexcept;
	void name(std::string& name_) noexcept;

	std::string group() const noexcept;
	void group(std::string& group_) noexcept;

	Army army() const noexcept;
	void army(Army army) noexcept;

	bool dropped() const noexcept;
	void dropped(bool drop) noexcept;

	// Need to set this fn call as a callback, not done every timne we need it.
	std::vector<xWingMatch*> matches() const noexcept;

	int score() const noexcept;
	double sos() const noexcept;
	double score_average() const noexcept;
	int wins() const noexcept;
	int defeats() const noexcept;
	int byes() const noexcept;
	int mov() const noexcept;

private:
	bool mIsDropped{ false };
	bool mFirstRoundBye = false;

	std::string mGroup = "LaGuarida";
	std::string mName = "Jolaskide";

	Army mArmy = Army::Rebel;
	std::map<Army, int> mKillMap;
};